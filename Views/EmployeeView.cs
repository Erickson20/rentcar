﻿using Datos.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using Negocio.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentCar.Views
{
    public partial class EmployeeView : MaterialForm
    {
        public EmployeeView()
        {
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            InitializeComponent();
            this.MaximizeBox = false;
        }

        private void EmployeeView_Load(object sender, EventArgs e)
        {
            setDataGrid();
            filter.DropDownStyle = ComboBoxStyle.DropDownList;
            filter.SelectedIndex = 0;
            seach.Enabled = false;
        }

        public void setDataGrid()
        {
            EmployeeController emplo = new EmployeeController();
            List<Employee> employees = emplo.getEmployees();
            GridEmployee.DataSource = employees.Select(o => new
            {
                Id = o.employeeId,
                Nombre = o.name,
                Apellidos = o.firstLastName + " " + o.secondLastName,
                Cedula = o.citizenId,
                horario = o.workshift
            }).ToList(); 

        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Create_EmployeeView emplo = new Create_EmployeeView();
            emplo.ShowDialog();
        }

        private void materialRaisedButton3_Click(object sender, EventArgs e)
        {
            EmployeeController emplo = new EmployeeController();
            int id = (int)GridEmployee.CurrentRow.Cells[0].Value;
            int result = emplo.DeleteEmployee(id);
            if (result > 0)
            {
                MessageBox.Show("El Empleado se borro correctamente", "Se borro correctamente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("El Empleado no se pudo borrar",
                                "Error al borrar los datos",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

            }
            setDataGrid();
        }

        private void materialRaisedButton2_Click(object sender, EventArgs e)
        {
            EmployeeController emplo = new EmployeeController();


            int id = (int)GridEmployee.CurrentRow.Cells[0].Value;
            var emploEdit = emplo.getEmployeeById(id);
            this.Hide();
            var employee = new Create_EmployeeView(emploEdit);
            employee.ShowDialog();
        }

        private void GridEmployee_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void filter_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (filter.SelectedItem.ToString() == "Nombre")
            {
                seach.Enabled = true;
                seach.Text = "buscar por nombre";
            }
            else if (filter.SelectedItem.ToString() == "Cedula")
            {
                seach.Enabled = true;
                seach.Text = "buscar por cedula";
            }
            else
            {
                seach.Enabled = false;
            }
        }

        private void seach_TextChanged(object sender, EventArgs e)
        {
            EmployeeController c = new EmployeeController();
            List<Employee> emplo = c.getEmployeeFilter(filter.SelectedItem.ToString(), seach.Text);

            GridEmployee.DataSource = emplo;
        }
    }
}
