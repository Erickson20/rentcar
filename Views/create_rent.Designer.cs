﻿namespace RentCar.Views
{
    partial class create_rent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.clientSelect = new System.Windows.Forms.ComboBox();
            this.dateRetorned = new System.Windows.Forms.DateTimePicker();
            this.days = new System.Windows.Forms.TextBox();
            this.payfordays = new System.Windows.Forms.TextBox();
            this.carSelect = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.employee = new System.Windows.Forms.TextBox();
            this.state = new System.Windows.Forms.ComboBox();
            this.comment = new System.Windows.Forms.RichTextBox();
            this.materialRaisedButton1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // clientSelect
            // 
            this.clientSelect.FormattingEnabled = true;
            this.clientSelect.Location = new System.Drawing.Point(12, 94);
            this.clientSelect.Name = "clientSelect";
            this.clientSelect.Size = new System.Drawing.Size(177, 21);
            this.clientSelect.TabIndex = 0;
            this.clientSelect.Text = "Cliente";
            // 
            // dateRetorned
            // 
            this.dateRetorned.Location = new System.Drawing.Point(12, 200);
            this.dateRetorned.Name = "dateRetorned";
            this.dateRetorned.Size = new System.Drawing.Size(381, 20);
            this.dateRetorned.TabIndex = 1;
            // 
            // days
            // 
            this.days.Location = new System.Drawing.Point(216, 94);
            this.days.Name = "days";
            this.days.Size = new System.Drawing.Size(177, 20);
            this.days.TabIndex = 2;
            this.days.Text = "Dias";
            this.days.MouseClick += new System.Windows.Forms.MouseEventHandler(this.days_MouseClick);
            this.days.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.days_KeyPress);
            // 
            // payfordays
            // 
            this.payfordays.Location = new System.Drawing.Point(43, 140);
            this.payfordays.Name = "payfordays";
            this.payfordays.Size = new System.Drawing.Size(146, 20);
            this.payfordays.TabIndex = 3;
            this.payfordays.Text = "Costo X dia";
            this.payfordays.MouseClick += new System.Windows.Forms.MouseEventHandler(this.payfordays_MouseClick);
            this.payfordays.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.payfordays_KeyPress);
            // 
            // carSelect
            // 
            this.carSelect.FormattingEnabled = true;
            this.carSelect.Location = new System.Drawing.Point(216, 140);
            this.carSelect.Name = "carSelect";
            this.carSelect.Size = new System.Drawing.Size(177, 21);
            this.carSelect.TabIndex = 4;
            this.carSelect.Text = "Vehiculos";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 181);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Fecha de Retorno";
            // 
            // employee
            // 
            this.employee.Location = new System.Drawing.Point(12, 249);
            this.employee.Name = "employee";
            this.employee.Size = new System.Drawing.Size(177, 20);
            this.employee.TabIndex = 6;
            this.employee.Text = "Empleado";
            // 
            // state
            // 
            this.state.FormattingEnabled = true;
            this.state.Items.AddRange(new object[] {
            "Activo",
            "Inactivo"});
            this.state.Location = new System.Drawing.Point(216, 248);
            this.state.Name = "state";
            this.state.Size = new System.Drawing.Size(177, 21);
            this.state.TabIndex = 7;
            this.state.Text = "Estado";
            // 
            // comment
            // 
            this.comment.Location = new System.Drawing.Point(16, 291);
            this.comment.Name = "comment";
            this.comment.Size = new System.Drawing.Size(377, 74);
            this.comment.TabIndex = 8;
            this.comment.Text = "Comentario";
            this.comment.MouseClick += new System.Windows.Forms.MouseEventHandler(this.comment_MouseClick);
            // 
            // materialRaisedButton1
            // 
            this.materialRaisedButton1.Depth = 0;
            this.materialRaisedButton1.Location = new System.Drawing.Point(156, 386);
            this.materialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton1.Name = "materialRaisedButton1";
            this.materialRaisedButton1.Primary = true;
            this.materialRaisedButton1.Size = new System.Drawing.Size(75, 23);
            this.materialRaisedButton1.TabIndex = 9;
            this.materialRaisedButton1.Text = "Guardar";
            this.materialRaisedButton1.UseVisualStyleBackColor = true;
            this.materialRaisedButton1.Click += new System.EventHandler(this.materialRaisedButton1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 143);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "RD$";
            // 
            // create_rent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(418, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.materialRaisedButton1);
            this.Controls.Add(this.comment);
            this.Controls.Add(this.state);
            this.Controls.Add(this.employee);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.carSelect);
            this.Controls.Add(this.payfordays);
            this.Controls.Add(this.days);
            this.Controls.Add(this.dateRetorned);
            this.Controls.Add(this.clientSelect);
            this.Name = "create_rent";
            this.Text = "create_rent";
            this.Load += new System.EventHandler(this.create_rent_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox clientSelect;
        private System.Windows.Forms.DateTimePicker dateRetorned;
        private System.Windows.Forms.TextBox days;
        private System.Windows.Forms.TextBox payfordays;
        private System.Windows.Forms.ComboBox carSelect;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox employee;
        private System.Windows.Forms.ComboBox state;
        private System.Windows.Forms.RichTextBox comment;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton1;
        private System.Windows.Forms.Label label2;
    }
}