﻿using MaterialSkin;
using MaterialSkin.Controls;
using RentCar.Controllers;
using Datos.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RentCar.Views
{
    public partial class RentView : MaterialForm
    {
        public Employee _user;
        public RentView(Employee user)
        {
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            InitializeComponent();
            this.MaximizeBox = false;
            this._user = user;
        }

        private void Rent_Load(object sender, EventArgs e)
        {
            setDataGrid();
        }
        public void setDataGrid()
        {
            RentController rents = new RentController();
            List<Rent> r = rents.getRents();
            GridRent.DataSource = r.Select(o => new
            {
                Id = o.rentId,
                Renta = o.dateRent,
                Dias = o.days ,
                Retorno = o.dateReturn,
                CostoXDia = o.costByDay
            }).ToList(); 

        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            create_rent rent = new create_rent(_user);
            rent.ShowDialog();
        }

        private void materialRaisedButton3_Click(object sender, EventArgs e)
        {
            int id = (int)GridRent.CurrentRow.Cells[0].Value;
            RentController rc = new RentController();
            var rent = rc.getRentById(id);
            rent.returned = true;
            Create_inspection ci = new Create_inspection(rent);
            ci.ShowDialog();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void seach_TextChanged(object sender, EventArgs e)
        {
            RentController c = new RentController();
            var InputSeach = seach.Text == "" ? "0" : seach.Text;
            List<Rent> r = c.getRentsFilter(Int32.Parse(InputSeach));

            GridRent.DataSource = r.Select(o => new
            {
                Id = o.rentId,
                Renta = o.dateRent,
                Dias = o.days,
                Retorno = o.dateReturn,
                CostoXDia = o.costByDay
            }).ToList();
            ;
        }

        private void seach_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            seach.Text = "";
        }
    }
}
