﻿namespace RentCar.Views
{
    partial class InspectionOut
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tyreFrontLeft = new MaterialSkin.Controls.MaterialCheckBox();
            this.tyreFrontRight = new MaterialSkin.Controls.MaterialCheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.hasScratches = new MaterialSkin.Controls.MaterialCheckBox();
            this.hasResponseTire = new MaterialSkin.Controls.MaterialCheckBox();
            this.hasHydraulicJack = new MaterialSkin.Controls.MaterialCheckBox();
            this.hasCrystalCrashes = new MaterialSkin.Controls.MaterialCheckBox();
            this.tyreBacktRigth = new MaterialSkin.Controls.MaterialCheckBox();
            this.tyreBacktLeft = new MaterialSkin.Controls.MaterialCheckBox();
            this.amount = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tyreFrontLeft
            // 
            this.tyreFrontLeft.AutoSize = true;
            this.tyreFrontLeft.Depth = 0;
            this.tyreFrontLeft.Font = new System.Drawing.Font("Roboto", 10F);
            this.tyreFrontLeft.Location = new System.Drawing.Point(193, 27);
            this.tyreFrontLeft.Margin = new System.Windows.Forms.Padding(0);
            this.tyreFrontLeft.MouseLocation = new System.Drawing.Point(-1, -1);
            this.tyreFrontLeft.MouseState = MaterialSkin.MouseState.HOVER;
            this.tyreFrontLeft.Name = "tyreFrontLeft";
            this.tyreFrontLeft.Ripple = true;
            this.tyreFrontLeft.Size = new System.Drawing.Size(139, 30);
            this.tyreFrontLeft.TabIndex = 7;
            this.tyreFrontLeft.Text = "Goma i. delantera";
            this.tyreFrontLeft.UseVisualStyleBackColor = true;
            // 
            // tyreFrontRight
            // 
            this.tyreFrontRight.AutoSize = true;
            this.tyreFrontRight.Depth = 0;
            this.tyreFrontRight.Font = new System.Drawing.Font("Roboto", 10F);
            this.tyreFrontRight.Location = new System.Drawing.Point(3, 27);
            this.tyreFrontRight.Margin = new System.Windows.Forms.Padding(0);
            this.tyreFrontRight.MouseLocation = new System.Drawing.Point(-1, -1);
            this.tyreFrontRight.MouseState = MaterialSkin.MouseState.HOVER;
            this.tyreFrontRight.Name = "tyreFrontRight";
            this.tyreFrontRight.Ripple = true;
            this.tyreFrontRight.Size = new System.Drawing.Size(143, 30);
            this.tyreFrontRight.TabIndex = 6;
            this.tyreFrontRight.Text = "Goma d. delantera";
            this.tyreFrontRight.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tyreFrontRight);
            this.groupBox1.Controls.Add(this.tyreFrontLeft);
            this.groupBox1.Location = new System.Drawing.Point(12, 193);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(365, 132);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Estado de gomas";
            // 
            // hasScratches
            // 
            this.hasScratches.AutoSize = true;
            this.hasScratches.Depth = 0;
            this.hasScratches.Font = new System.Drawing.Font("Roboto", 10F);
            this.hasScratches.Location = new System.Drawing.Point(12, 82);
            this.hasScratches.Margin = new System.Windows.Forms.Padding(0);
            this.hasScratches.MouseLocation = new System.Drawing.Point(-1, -1);
            this.hasScratches.MouseState = MaterialSkin.MouseState.HOVER;
            this.hasScratches.Name = "hasScratches";
            this.hasScratches.Ripple = true;
            this.hasScratches.Size = new System.Drawing.Size(129, 30);
            this.hasScratches.TabIndex = 14;
            this.hasScratches.Text = "Tiene rayaduras";
            this.hasScratches.UseVisualStyleBackColor = true;
            // 
            // hasResponseTire
            // 
            this.hasResponseTire.AutoSize = true;
            this.hasResponseTire.Depth = 0;
            this.hasResponseTire.Font = new System.Drawing.Font("Roboto", 10F);
            this.hasResponseTire.Location = new System.Drawing.Point(12, 146);
            this.hasResponseTire.Margin = new System.Windows.Forms.Padding(0);
            this.hasResponseTire.MouseLocation = new System.Drawing.Point(-1, -1);
            this.hasResponseTire.MouseState = MaterialSkin.MouseState.HOVER;
            this.hasResponseTire.Name = "hasResponseTire";
            this.hasResponseTire.Ripple = true;
            this.hasResponseTire.Size = new System.Drawing.Size(162, 30);
            this.hasResponseTire.TabIndex = 16;
            this.hasResponseTire.Text = "Tiene Goma repuesta";
            this.hasResponseTire.UseVisualStyleBackColor = true;
            // 
            // hasHydraulicJack
            // 
            this.hasHydraulicJack.AutoSize = true;
            this.hasHydraulicJack.Depth = 0;
            this.hasHydraulicJack.Font = new System.Drawing.Font("Roboto", 10F);
            this.hasHydraulicJack.Location = new System.Drawing.Point(205, 146);
            this.hasHydraulicJack.Margin = new System.Windows.Forms.Padding(0);
            this.hasHydraulicJack.MouseLocation = new System.Drawing.Point(-1, -1);
            this.hasHydraulicJack.MouseState = MaterialSkin.MouseState.HOVER;
            this.hasHydraulicJack.Name = "hasHydraulicJack";
            this.hasHydraulicJack.Ripple = true;
            this.hasHydraulicJack.Size = new System.Drawing.Size(97, 30);
            this.hasHydraulicJack.TabIndex = 17;
            this.hasHydraulicJack.Text = "Tiene Gato";
            this.hasHydraulicJack.UseVisualStyleBackColor = true;
            // 
            // hasCrystalCrashes
            // 
            this.hasCrystalCrashes.AutoSize = true;
            this.hasCrystalCrashes.Depth = 0;
            this.hasCrystalCrashes.Font = new System.Drawing.Font("Roboto", 10F);
            this.hasCrystalCrashes.Location = new System.Drawing.Point(205, 82);
            this.hasCrystalCrashes.Margin = new System.Windows.Forms.Padding(0);
            this.hasCrystalCrashes.MouseLocation = new System.Drawing.Point(-1, -1);
            this.hasCrystalCrashes.MouseState = MaterialSkin.MouseState.HOVER;
            this.hasCrystalCrashes.Name = "hasCrystalCrashes";
            this.hasCrystalCrashes.Ripple = true;
            this.hasCrystalCrashes.Size = new System.Drawing.Size(172, 30);
            this.hasCrystalCrashes.TabIndex = 18;
            this.hasCrystalCrashes.Text = "Tiene roturas de cristal";
            this.hasCrystalCrashes.UseVisualStyleBackColor = true;
            // 
            // tyreBacktRigth
            // 
            this.tyreBacktRigth.AutoSize = true;
            this.tyreBacktRigth.Depth = 0;
            this.tyreBacktRigth.Font = new System.Drawing.Font("Roboto", 10F);
            this.tyreBacktRigth.Location = new System.Drawing.Point(15, 263);
            this.tyreBacktRigth.Margin = new System.Windows.Forms.Padding(0);
            this.tyreBacktRigth.MouseLocation = new System.Drawing.Point(-1, -1);
            this.tyreBacktRigth.MouseState = MaterialSkin.MouseState.HOVER;
            this.tyreBacktRigth.Name = "tyreBacktRigth";
            this.tyreBacktRigth.Ripple = true;
            this.tyreBacktRigth.Size = new System.Drawing.Size(128, 30);
            this.tyreBacktRigth.TabIndex = 19;
            this.tyreBacktRigth.Text = "Goma d. trasera";
            this.tyreBacktRigth.UseVisualStyleBackColor = true;
            // 
            // tyreBacktLeft
            // 
            this.tyreBacktLeft.AutoSize = true;
            this.tyreBacktLeft.Depth = 0;
            this.tyreBacktLeft.Font = new System.Drawing.Font("Roboto", 10F);
            this.tyreBacktLeft.Location = new System.Drawing.Point(205, 263);
            this.tyreBacktLeft.Margin = new System.Windows.Forms.Padding(0);
            this.tyreBacktLeft.MouseLocation = new System.Drawing.Point(-1, -1);
            this.tyreBacktLeft.MouseState = MaterialSkin.MouseState.HOVER;
            this.tyreBacktLeft.Name = "tyreBacktLeft";
            this.tyreBacktLeft.Ripple = true;
            this.tyreBacktLeft.Size = new System.Drawing.Size(124, 30);
            this.tyreBacktLeft.TabIndex = 20;
            this.tyreBacktLeft.Text = "Goma i. trasera";
            this.tyreBacktLeft.UseVisualStyleBackColor = true;
            // 
            // amount
            // 
            this.amount.Location = new System.Drawing.Point(12, 357);
            this.amount.Name = "amount";
            this.amount.Size = new System.Drawing.Size(131, 20);
            this.amount.TabIndex = 22;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 338);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "Combustible";
            // 
            // InspectionOut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 389);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.amount);
            this.Controls.Add(this.tyreBacktLeft);
            this.Controls.Add(this.tyreBacktRigth);
            this.Controls.Add(this.hasCrystalCrashes);
            this.Controls.Add(this.hasHydraulicJack);
            this.Controls.Add(this.hasResponseTire);
            this.Controls.Add(this.hasScratches);
            this.Controls.Add(this.groupBox1);
            this.Name = "InspectionOut";
            this.Text = "InspectionOut";
            this.Load += new System.EventHandler(this.InspectionOut_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialCheckBox tyreFrontLeft;
        private MaterialSkin.Controls.MaterialCheckBox tyreFrontRight;
        private System.Windows.Forms.GroupBox groupBox1;
        private MaterialSkin.Controls.MaterialCheckBox hasScratches;
        private MaterialSkin.Controls.MaterialCheckBox hasResponseTire;
        private MaterialSkin.Controls.MaterialCheckBox hasHydraulicJack;
        private MaterialSkin.Controls.MaterialCheckBox hasCrystalCrashes;
        private MaterialSkin.Controls.MaterialCheckBox tyreBacktRigth;
        private MaterialSkin.Controls.MaterialCheckBox tyreBacktLeft;
        private System.Windows.Forms.TextBox amount;
        private System.Windows.Forms.Label label1;
    }
}