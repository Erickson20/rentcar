﻿using Datos.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using Negocio.Controllers;
using RentCar.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentCar.Views
{
    public partial class Create_EmployeeView : MaterialForm
    {
        public bool btnUpdate = false;
        public int idEmployee = 0;
        public Create_EmployeeView(Employee emplo = null)
        {
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            InitializeComponent();
            this.MaximizeBox = false;
            if (emplo != null)
            {
                E_name.Text = emplo.name;
                E_fLastName.Text = emplo.firstLastName;
                E_sLastName.Text = emplo.secondLastName;
                E_citizen.Text = emplo.citizenId;
                E_date.Text = emplo.hireDate.ToString();
                E_workshift.Text = emplo.workshift;
                E_state.Text = emplo.state;
                E_Comission.Text = emplo.commissionPorcent.ToString();

                btnUpdate = true;
                idEmployee = emplo.employeeId;
            }
        }

        public void saveEmployee(Employee emplo, EmployeeController controller ,User user)
        {
            UserController u = new UserController();
            emplo.name = E_name.Text;
            emplo.firstLastName = E_fLastName.Text;
            emplo.secondLastName = E_sLastName.Text;
            emplo.citizenId = E_citizen.Text;
            emplo.hireDate = E_date.Value;
            emplo.workshift = E_workshift.SelectedItem.ToString();
            emplo.state = E_state.SelectedItem.ToString();
            emplo.commissionPorcent = float.Parse(E_Comission.Text);

            emplo.user = user;
            

            int result = controller.CreateEmployee(emplo);
            if (result > 0)
            {
                MessageBox.Show("El Empleado se agrego correctamente", "Se guardo correctamente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("El Empleado no se pudo agregar",
                                "Error al guardar los datos",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

            }
            this.Hide();
        }

        public void UpdateEmployee(Employee emplo, EmployeeController controller, int id)
        {

            emplo.name = E_name.Text;
            emplo.firstLastName = E_fLastName.Text;
            emplo.secondLastName = E_sLastName.Text;
            emplo.citizenId = E_citizen.Text;
            emplo.hireDate = E_date.Value;
            emplo.workshift = E_workshift.SelectedItem.ToString();
            emplo.state = E_state.SelectedItem.ToString();
            emplo.commissionPorcent = float.Parse(E_Comission.Text);
            int result = controller.UpdateEmployee(emplo, id);

            if (result > 0)
            {
                MessageBox.Show("El Empleado se actualizo correctamente", "Se Aactualizo correctamente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("El Empleado no se pudo actualizar",
                                "Error al Actualizar los datos",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

            }
            this.Hide();

        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            EmployeeController controller = new EmployeeController();
            Employee emplo = new Employee();


            if (btnUpdate)
            {
                UpdateEmployee(emplo, controller, idEmployee);
            }
            else
            {
                //saveEmployee(emplo, controller);
            }
            EmployeeView employee = new EmployeeView();
            employee.ShowDialog();
        }

        private void name_TextChanged(object sender, EventArgs e)
        {
            ;
        }

        private void fLastname_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void slastname_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void citizen_TextChanged(object sender, EventArgs e)
        {
            E_citizen.Text = "";
        }

        private void comission_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void date_ValueChanged(object sender, EventArgs e)
        {

        }

        private void Create_EmployeeView_Load(object sender, EventArgs e)
        {
            if (btnUpdate)
            {
                email.Visible = false;
                pass.Visible = false;
            }
            E_state.DropDownStyle = ComboBoxStyle.DropDownList;
            E_state.SelectedIndex = 0;
            E_workshift.DropDownStyle = ComboBoxStyle.DropDownList;
            E_workshift.SelectedIndex = 0;
        }

        private void materialRaisedButton2_Click(object sender, EventArgs e)
        {
            EmployeeController controller = new EmployeeController();
    
            User user = new User { email = email.Text, roleId = 2, password = pass.Text, firstName = E_name.Text, lastName = E_fLastName.Text };
            Employee emplo = new Employee();


            if (btnUpdate)
            {
                UpdateEmployee(emplo, controller, idEmployee);
                email.Visible = false;
                pass.Visible = false;
            }
            else
            {
              
                saveEmployee(emplo, controller, user);
            }
            EmployeeView employee = new EmployeeView();
            employee.ShowDialog();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void E_state_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void E_workshift_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void E_date_ValueChanged(object sender, EventArgs e)
        {

        }

        private void E_Comission_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void E_citizen_TextChanged(object sender, EventArgs e)
        {

        }

        private void E_name_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void E_sLastName_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void E_fLastName_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void E_Comission_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Verify that the pressed key isn't CTRL or any non-numeric digit
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                
                
                    e.Handled = true;
                
                
            }

            // If you want, you can allow decimal (float) numbers
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void E_Comission_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            E_Comission.Text = "";
        }

        private void email_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void pass_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void E_name_MouseClick(object sender, MouseEventArgs e)
        {
            E_name.Text = "";
        }

        private void E_fLastName_MouseClick(object sender, MouseEventArgs e)
        {
            E_fLastName.Text = "";
        }

        private void E_sLastName_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void E_sLastName_MouseClick(object sender, MouseEventArgs e)
        {
            E_sLastName.Text = "";
        }

        private void email_MouseClick(object sender, MouseEventArgs e)
        {
            email.Text = "";
        }

        private void pass_MouseClick(object sender, MouseEventArgs e)
        {
            pass.Text = "";
        }
    }
}
