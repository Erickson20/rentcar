﻿namespace RentCar.Views
{
    partial class create_settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.descriptions = new System.Windows.Forms.TextBox();
            this.state = new System.Windows.Forms.ComboBox();
            this.materialRaisedButton1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.brand = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // descriptions
            // 
            this.descriptions.Location = new System.Drawing.Point(12, 108);
            this.descriptions.Name = "descriptions";
            this.descriptions.Size = new System.Drawing.Size(143, 20);
            this.descriptions.TabIndex = 0;
            this.descriptions.Text = "descripcion";
            // 
            // state
            // 
            this.state.FormattingEnabled = true;
            this.state.Items.AddRange(new object[] {
            "Activo",
            "Inactivo"});
            this.state.Location = new System.Drawing.Point(174, 108);
            this.state.Name = "state";
            this.state.Size = new System.Drawing.Size(161, 21);
            this.state.TabIndex = 1;
            this.state.Text = "Estado";
            // 
            // materialRaisedButton1
            // 
            this.materialRaisedButton1.Depth = 0;
            this.materialRaisedButton1.Location = new System.Drawing.Point(129, 184);
            this.materialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton1.Name = "materialRaisedButton1";
            this.materialRaisedButton1.Primary = true;
            this.materialRaisedButton1.Size = new System.Drawing.Size(75, 23);
            this.materialRaisedButton1.TabIndex = 2;
            this.materialRaisedButton1.Text = "Guardar";
            this.materialRaisedButton1.UseVisualStyleBackColor = true;
            this.materialRaisedButton1.Click += new System.EventHandler(this.materialRaisedButton1_Click);
            // 
            // brand
            // 
            this.brand.FormattingEnabled = true;
            this.brand.Items.AddRange(new object[] {
            "Activo",
            "Inactivo"});
            this.brand.Location = new System.Drawing.Point(12, 147);
            this.brand.Name = "brand";
            this.brand.Size = new System.Drawing.Size(143, 21);
            this.brand.TabIndex = 3;
            this.brand.Text = "Marca";
            // 
            // create_settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 230);
            this.Controls.Add(this.brand);
            this.Controls.Add(this.materialRaisedButton1);
            this.Controls.Add(this.state);
            this.Controls.Add(this.descriptions);
            this.Name = "create_settings";
            this.Text = "create_settings";
            this.Load += new System.EventHandler(this.create_settings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox descriptions;
        private System.Windows.Forms.ComboBox state;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton1;
        private System.Windows.Forms.ComboBox brand;
    }
}