﻿namespace RentCar.Views
{
    partial class Create_EmployeeView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.E_fLastName = new System.Windows.Forms.TextBox();
            this.E_sLastName = new System.Windows.Forms.TextBox();
            this.E_name = new System.Windows.Forms.TextBox();
            this.E_date = new System.Windows.Forms.DateTimePicker();
            this.E_workshift = new System.Windows.Forms.ComboBox();
            this.E_state = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.materialRaisedButton2 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.email = new System.Windows.Forms.TextBox();
            this.pass = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.E_citizen = new System.Windows.Forms.MaskedTextBox();
            this.E_Comission = new System.Windows.Forms.MaskedTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // E_fLastName
            // 
            this.E_fLastName.Location = new System.Drawing.Point(12, 137);
            this.E_fLastName.Name = "E_fLastName";
            this.E_fLastName.Size = new System.Drawing.Size(143, 20);
            this.E_fLastName.TabIndex = 0;
            this.E_fLastName.Text = "1er Apellido";
            this.E_fLastName.MouseClick += new System.Windows.Forms.MouseEventHandler(this.E_fLastName_MouseClick);
            this.E_fLastName.TextChanged += new System.EventHandler(this.E_fLastName_TextChanged);
            // 
            // E_sLastName
            // 
            this.E_sLastName.Location = new System.Drawing.Point(179, 137);
            this.E_sLastName.Name = "E_sLastName";
            this.E_sLastName.Size = new System.Drawing.Size(143, 20);
            this.E_sLastName.TabIndex = 1;
            this.E_sLastName.Text = "2do Apellido";
            this.E_sLastName.MouseClick += new System.Windows.Forms.MouseEventHandler(this.E_sLastName_MouseClick);
            this.E_sLastName.TextChanged += new System.EventHandler(this.E_sLastName_TextChanged);
            this.E_sLastName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.E_sLastName_KeyPress);
            // 
            // E_name
            // 
            this.E_name.Location = new System.Drawing.Point(12, 100);
            this.E_name.Name = "E_name";
            this.E_name.Size = new System.Drawing.Size(310, 20);
            this.E_name.TabIndex = 2;
            this.E_name.Text = "Nombre";
            this.E_name.MouseClick += new System.Windows.Forms.MouseEventHandler(this.E_name_MouseClick);
            this.E_name.TextChanged += new System.EventHandler(this.E_name_TextChanged);
            // 
            // E_date
            // 
            this.E_date.Location = new System.Drawing.Point(12, 281);
            this.E_date.Name = "E_date";
            this.E_date.Size = new System.Drawing.Size(309, 20);
            this.E_date.TabIndex = 5;
            this.E_date.ValueChanged += new System.EventHandler(this.E_date_ValueChanged);
            // 
            // E_workshift
            // 
            this.E_workshift.FormattingEnabled = true;
            this.E_workshift.Items.AddRange(new object[] {
            "matutina",
            "nocturna"});
            this.E_workshift.Location = new System.Drawing.Point(13, 216);
            this.E_workshift.Name = "E_workshift";
            this.E_workshift.Size = new System.Drawing.Size(142, 21);
            this.E_workshift.TabIndex = 6;
            this.E_workshift.Text = "Tanda Labor";
            this.E_workshift.SelectedIndexChanged += new System.EventHandler(this.E_workshift_SelectedIndexChanged);
            // 
            // E_state
            // 
            this.E_state.FormattingEnabled = true;
            this.E_state.Items.AddRange(new object[] {
            "Activo",
            "Inactivo"});
            this.E_state.Location = new System.Drawing.Point(179, 215);
            this.E_state.Name = "E_state";
            this.E_state.Size = new System.Drawing.Size(143, 21);
            this.E_state.TabIndex = 7;
            this.E_state.Text = "Estado";
            this.E_state.SelectedIndexChanged += new System.EventHandler(this.E_state_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 253);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Fecha de Ingreso";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // materialRaisedButton2
            // 
            this.materialRaisedButton2.Depth = 0;
            this.materialRaisedButton2.Location = new System.Drawing.Point(126, 373);
            this.materialRaisedButton2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton2.Name = "materialRaisedButton2";
            this.materialRaisedButton2.Primary = true;
            this.materialRaisedButton2.Size = new System.Drawing.Size(75, 23);
            this.materialRaisedButton2.TabIndex = 10;
            this.materialRaisedButton2.Text = "Guardar";
            this.materialRaisedButton2.UseVisualStyleBackColor = true;
            this.materialRaisedButton2.Click += new System.EventHandler(this.materialRaisedButton2_Click);
            // 
            // email
            // 
            this.email.Location = new System.Drawing.Point(12, 338);
            this.email.Name = "email";
            this.email.Size = new System.Drawing.Size(143, 20);
            this.email.TabIndex = 11;
            this.email.Text = "Email";
            this.email.MouseClick += new System.Windows.Forms.MouseEventHandler(this.email_MouseClick);
            this.email.TextChanged += new System.EventHandler(this.email_TextChanged);
            // 
            // pass
            // 
            this.pass.Location = new System.Drawing.Point(179, 338);
            this.pass.Name = "pass";
            this.pass.Size = new System.Drawing.Size(143, 20);
            this.pass.TabIndex = 12;
            this.pass.Text = "Password";
            this.pass.UseSystemPasswordChar = true;
            this.pass.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pass_MouseClick);
            this.pass.TextChanged += new System.EventHandler(this.pass_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 311);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Usuario para registro";
            // 
            // E_citizen
            // 
            this.E_citizen.Location = new System.Drawing.Point(15, 175);
            this.E_citizen.Mask = "999-9999999-9";
            this.E_citizen.Name = "E_citizen";
            this.E_citizen.Size = new System.Drawing.Size(140, 20);
            this.E_citizen.TabIndex = 14;
            // 
            // E_Comission
            // 
            this.E_Comission.Location = new System.Drawing.Point(179, 175);
            this.E_Comission.Mask = "99";
            this.E_Comission.Name = "E_Comission";
            this.E_Comission.Size = new System.Drawing.Size(53, 20);
            this.E_Comission.TabIndex = 15;
            this.E_Comission.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.E_Comission_MaskInputRejected);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(238, 179);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 16);
            this.label4.TabIndex = 16;
            this.label4.Text = "%";
            // 
            // Create_EmployeeView
            // 
            this.ClientSize = new System.Drawing.Size(349, 408);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.E_Comission);
            this.Controls.Add(this.E_citizen);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pass);
            this.Controls.Add(this.email);
            this.Controls.Add(this.materialRaisedButton2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.E_state);
            this.Controls.Add(this.E_workshift);
            this.Controls.Add(this.E_date);
            this.Controls.Add(this.E_name);
            this.Controls.Add(this.E_sLastName);
            this.Controls.Add(this.E_fLastName);
            this.Name = "Create_EmployeeView";
            this.Load += new System.EventHandler(this.Create_EmployeeView_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.TextBox citizen;
        private System.Windows.Forms.TextBox comission;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton1;
        private System.Windows.Forms.ComboBox workshift;
        private System.Windows.Forms.ComboBox state;
        private System.Windows.Forms.DateTimePicker date;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox fLastname;
        private System.Windows.Forms.TextBox slastname;
        private System.Windows.Forms.TextBox E_fLastName;
        private System.Windows.Forms.TextBox E_sLastName;
        private System.Windows.Forms.TextBox E_name;
        private System.Windows.Forms.DateTimePicker E_date;
        private System.Windows.Forms.ComboBox E_workshift;
        private System.Windows.Forms.ComboBox E_state;
        private System.Windows.Forms.Label label2;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton2;
        private System.Windows.Forms.TextBox email;
        private System.Windows.Forms.TextBox pass;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox E_citizen;
        private System.Windows.Forms.MaskedTextBox E_Comission;
        private System.Windows.Forms.Label label4;
    }
}