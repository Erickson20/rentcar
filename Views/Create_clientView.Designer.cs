﻿namespace RentCar.Views
{
    partial class Create_clientView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.materialRaisedButton2 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.state = new System.Windows.Forms.ComboBox();
            this.persontype = new System.Windows.Forms.ComboBox();
            this.CreditCard = new System.Windows.Forms.TextBox();
            this.name = new System.Windows.Forms.TextBox();
            this.slastname = new System.Windows.Forms.TextBox();
            this.flastname = new System.Windows.Forms.TextBox();
            this.citizen = new System.Windows.Forms.MaskedTextBox();
            this.limitcredit = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // materialRaisedButton2
            // 
            this.materialRaisedButton2.Depth = 0;
            this.materialRaisedButton2.Location = new System.Drawing.Point(124, 285);
            this.materialRaisedButton2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton2.Name = "materialRaisedButton2";
            this.materialRaisedButton2.Primary = true;
            this.materialRaisedButton2.Size = new System.Drawing.Size(75, 23);
            this.materialRaisedButton2.TabIndex = 20;
            this.materialRaisedButton2.Text = "Guardar";
            this.materialRaisedButton2.UseVisualStyleBackColor = true;
            this.materialRaisedButton2.Click += new System.EventHandler(this.materialRaisedButton2_Click);
            // 
            // state
            // 
            this.state.FormattingEnabled = true;
            this.state.Items.AddRange(new object[] {
            "Activo",
            "Inactivo"});
            this.state.Location = new System.Drawing.Point(179, 202);
            this.state.Name = "state";
            this.state.Size = new System.Drawing.Size(143, 21);
            this.state.TabIndex = 18;
            this.state.Text = "Estado";
            // 
            // persontype
            // 
            this.persontype.FormattingEnabled = true;
            this.persontype.Items.AddRange(new object[] {
            "Persona Fisica",
            "Persona Juridica"});
            this.persontype.Location = new System.Drawing.Point(13, 203);
            this.persontype.Name = "persontype";
            this.persontype.Size = new System.Drawing.Size(142, 21);
            this.persontype.TabIndex = 17;
            this.persontype.Text = "Tipo de Persona";
            this.persontype.SelectedIndexChanged += new System.EventHandler(this.persontype_SelectedIndexChanged);
            // 
            // CreditCard
            // 
            this.CreditCard.Location = new System.Drawing.Point(179, 163);
            this.CreditCard.Name = "CreditCard";
            this.CreditCard.Size = new System.Drawing.Size(143, 20);
            this.CreditCard.TabIndex = 15;
            this.CreditCard.Text = "Tarjeta de Credito";
            this.CreditCard.MouseClick += new System.Windows.Forms.MouseEventHandler(this.CreditCard_MouseClick);
            this.CreditCard.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CreditCard_KeyPress);
            // 
            // name
            // 
            this.name.Location = new System.Drawing.Point(12, 87);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(310, 20);
            this.name.TabIndex = 13;
            this.name.Text = "Nombre";
            this.name.MouseClick += new System.Windows.Forms.MouseEventHandler(this.name_MouseClick);
            // 
            // slastname
            // 
            this.slastname.Location = new System.Drawing.Point(179, 124);
            this.slastname.Name = "slastname";
            this.slastname.Size = new System.Drawing.Size(143, 20);
            this.slastname.TabIndex = 12;
            this.slastname.Text = "2do Apellido";
            this.slastname.MouseClick += new System.Windows.Forms.MouseEventHandler(this.slastname_MouseClick);
            // 
            // flastname
            // 
            this.flastname.Location = new System.Drawing.Point(12, 124);
            this.flastname.Name = "flastname";
            this.flastname.Size = new System.Drawing.Size(143, 20);
            this.flastname.TabIndex = 11;
            this.flastname.Text = "1er Apellido";
            this.flastname.MouseClick += new System.Windows.Forms.MouseEventHandler(this.flastname_MouseClick);
            // 
            // citizen
            // 
            this.citizen.Location = new System.Drawing.Point(13, 162);
            this.citizen.Mask = "000-0000000-0";
            this.citizen.Name = "citizen";
            this.citizen.Size = new System.Drawing.Size(142, 20);
            this.citizen.TabIndex = 22;
            // 
            // limitcredit
            // 
            this.limitcredit.HidePromptOnLeave = true;
            this.limitcredit.HideSelection = false;
            this.limitcredit.Location = new System.Drawing.Point(39, 253);
            this.limitcredit.Name = "limitcredit";
            this.limitcredit.PromptChar = 'd';
            this.limitcredit.ResetOnSpace = false;
            this.limitcredit.Size = new System.Drawing.Size(283, 20);
            this.limitcredit.TabIndex = 23;
            this.limitcredit.Text = "Limite de credito";
            this.limitcredit.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.limitcredit_MaskInputRejected);
            this.limitcredit.MouseClick += new System.Windows.Forms.MouseEventHandler(this.limitcredit_MouseClick);
            this.limitcredit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.limitcredit_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 256);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "RD$";
            // 
            // Create_clientView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(341, 345);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.limitcredit);
            this.Controls.Add(this.citizen);
            this.Controls.Add(this.materialRaisedButton2);
            this.Controls.Add(this.state);
            this.Controls.Add(this.persontype);
            this.Controls.Add(this.CreditCard);
            this.Controls.Add(this.name);
            this.Controls.Add(this.slastname);
            this.Controls.Add(this.flastname);
            this.Name = "Create_clientView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Create_clientView";
            this.Load += new System.EventHandler(this.Create_clientView_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton2;
        private System.Windows.Forms.ComboBox state;
        private System.Windows.Forms.ComboBox persontype;
        private System.Windows.Forms.TextBox CreditCard;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.TextBox slastname;
        private System.Windows.Forms.TextBox flastname;
        private System.Windows.Forms.MaskedTextBox citizen;
        private System.Windows.Forms.MaskedTextBox limitcredit;
        private System.Windows.Forms.Label label1;
    }
}