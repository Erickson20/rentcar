﻿using Datos.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using RentCar.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentCar.Views
{
    public partial class create_settings : MaterialForm
    {
        String _type;
        public create_settings(String type = null)
        {
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            InitializeComponent();
            this.MaximizeBox = false;
            this._type = type;
        }

        private void create_settings_Load(object sender, EventArgs e)
        {

            state.SelectedIndex = 0;
            state.DropDownStyle = ComboBoxStyle.DropDownList;
            if (_type == "model")
            {
                BrandController brandc = new BrandController();
                List<Brand> brands = brandc.getBrands();
                brand.DataSource = brands;
                brand.ValueMember = "brandId";
                brand.DisplayMember = "description";
                brand.Visible = true;
                brand.DropDownStyle = ComboBoxStyle.DropDownList;
            }
            else
            {
                brand.Visible = false;
            }
            

        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            
            
            

            if(_type == "brand")
            {
                saveBrand();
            }else if(_type == "carType")
            {
                saveCartType();
            }
            else
            {
                saveModel();
            }

            
        }


        public void saveBrand()
        {
            Brand b = new Brand();
            BrandController brand = new BrandController();
            b.description = descriptions.Text;
            b.state = state.SelectedItem.ToString();



            int result = brand.CreateBrand(b);
            if (result > 0)
            {
                MessageBox.Show("La Marca se agrego correctamente", "Se guardo correctamente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("La Marca no se pudo agregar",
                                "Error al guardar los datos",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

            }
            this.Hide();
        }

        public void saveCartType()
        {
            CarType c = new CarType();
            CarTypesController carTypes = new CarTypesController();
            c.description = descriptions.Text;
            c.state = state.SelectedItem.ToString();


           
            int result = carTypes.CreateCarTypes(c);
            if (result > 0)
            {
                MessageBox.Show("El tipo de vehiculo se agrego correctamente", "Se guardo correctamente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("El tipo de vehiculo no se pudo agregar",
                                "Error al guardar los datos",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

            }
            this.Hide();
        }




        public void saveModel()
        {
            Model m = new Model();
            ModelsController Model = new ModelsController();
           
            m.description = descriptions.Text;
            m.state = state.SelectedItem.ToString();
            m.brandId = Int32.Parse(brand.SelectedValue.ToString());



            int result = Model.CreateModel(m);
            if (result > 0)
            {
                MessageBox.Show("El Modelo se agrego correctamente", "Se guardo correctamente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("El Modelo no se pudo agregar",
                                "Error al guardar los datos",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

            }
            this.Hide();
        }
    }
}
