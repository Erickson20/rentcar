﻿using Datos.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using Negocio.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentCar.Views
{
    public partial class ClientView : MaterialForm
    {
        public ClientView()
        {
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            InitializeComponent();
            this.MaximizeBox = false; 
        }

        private void ClientView_Load(object sender, EventArgs e)
        {
            setDataGrid();
            filter.DropDownStyle = ComboBoxStyle.DropDownList;
            filter.SelectedIndex = 0;
            seach.Enabled = false;
        }
        public void setDataGrid()
        {
            ClientController client = new ClientController();
            List<Client> clients = client.GetClients();
           
            ClientGrid.DataSource = clients.Select(o => new
            { Id = o.clientId, Nombre = o.name,
              Apellidos = o.firstLastName + " " + o.secondLastName,
              Cedula = o.citizenId,
            Credito = o.creditLimit}).ToList();


        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            Create_clientView client = new Create_clientView();
            client.ShowDialog();

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void materialRaisedButton3_Click(object sender, EventArgs e)
        {
            ClientController client = new ClientController();
            int id = (int)ClientGrid.CurrentRow.Cells[0].Value;
            int result = client.DeleteClient(id);
            if (result > 0)
            {
                MessageBox.Show("El Cliente se borro correctamente", "Se borro correctamente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("El Cliente no se pudo borrar",
                                "Error al borrar los datos",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

            }
            setDataGrid();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void materialRaisedButton2_Click(object sender, EventArgs e)
        {
            ClientController client = new ClientController();


            int id = (int)ClientGrid.CurrentRow.Cells[0].Value;
            var clientEdit = client.GetClient(id);
            this.Hide();
            var cli = new Create_clientView(clientEdit);
            cli.ShowDialog();
        }

        private void seach_TextChanged(object sender, EventArgs e)
        {
            ClientController c = new ClientController();
            List<Client> clients = c.getClientFilter(filter.SelectedItem.ToString(), seach.Text);

            ClientGrid.DataSource = clients;
        }

        private void filter_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            if (filter.SelectedItem.ToString() == "Nombre")
            {
                seach.Enabled = true;
                seach.Text = "buscar por nombre";
            }
            else if (filter.SelectedItem.ToString() == "Cedula")
            {
                seach.Enabled = true;
                seach.Text = "buscar por cedula";
            }else
            {
                seach.Enabled = false;
            }
        }

        private void seach_MouseClick(object sender, MouseEventArgs e)
        {
            seach.Text = "";
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
