﻿using Datos.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using RentCar.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentCar
{
    public partial class Login : MaterialForm
    {
        public Login()
        {

            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            InitializeComponent();
            this.MaximizeBox = false;
        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            User user = new User();
            user.email = materialSingleLineTextField1.Text;
            //user.setPassword(materialSingleLineTextField2.Text);
            user.password = materialSingleLineTextField2.Text;
            UserController controller = new UserController();
            
            var emplo = controller.ValidateCredentials(user);
            if (emplo != null)
            {
                Menu menu = new Menu(emplo);
                menu.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Revise sus credenciales e intende nuevamente");
            }
            

        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void materialSingleLineTextField1_Click(object sender, EventArgs e)
        {
           
          
        }

        private void materialSingleLineTextField2_Click(object sender, EventArgs e)
        {
          
        }

        private void materialSingleLineTextField1_Enter(object sender, EventArgs e)
        {
            var email = materialSingleLineTextField1.Text;
            if (email == "EMAIL")
                materialSingleLineTextField1.Text = "";
            
        }

        private void materialSingleLineTextField1_Leave(object sender, EventArgs e)
        {
            var email = materialSingleLineTextField1.Text;
            if (email == "")
                materialSingleLineTextField1.Text = "EMAIL";
          
        }

        private void materialSingleLineTextField2_Enter(object sender, EventArgs e)
        {
            var pass = materialSingleLineTextField2.Text;
            if (pass == "PASSWORD")
                materialSingleLineTextField2.Text = "";
        
        }

        private void materialSingleLineTextField2_Leave(object sender, EventArgs e)
        {
            var pass = materialSingleLineTextField2.Text;
            if (pass == "")
                materialSingleLineTextField2.Text = "PASSWORD";
        }

        private void materialRaisedButton2_Click(object sender, EventArgs e)
        {
         
            UserController controller = new UserController();
            User user = new User();
            user.email = email.Text;
            user.firstName = firstName.Text;
            user.lastName = lastname.Text;
            user.password = password.Text;

            controller.CreateUser(user);
            materialTabSelector1.SelectNextControl(materialTabControl1,true,true,true,true);
        }

        private void firstName_Enter(object sender, EventArgs e)
        {
            if (firstName.Text == "Nombres")
                firstName.Text = "";
        }

        private void firstName_Leave(object sender, EventArgs e)
        {
            if (firstName.Text == "")
                firstName.Text = "Nombres";
        }

        private void lastname_Enter(object sender, EventArgs e)
        {
            if (lastname.Text == "Apellidos")
                lastname.Text = "";
        }

        private void email_Enter(object sender, EventArgs e)
        {
            if (email.Text == "Email")
                email.Text = "";
        }

        private void lastname_Leave(object sender, EventArgs e)
        {
            if (lastname.Text == "")
                lastname.Text = "Apellidos";
        }

        private void email_Leave(object sender, EventArgs e)
        {
            if (email.Text == "")
                email.Text = "Email";
        }

        private void password_Enter(object sender, EventArgs e)
        {
            if (password.Text == "PASSWORD")
                password.Text = "";
        }

        private void password_Leave(object sender, EventArgs e)
        {
            if (password.Text == "")
                password.Text = "PASSWORD";
        }

        private void materialTabSelector1_Click(object sender, EventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }
    }
}
