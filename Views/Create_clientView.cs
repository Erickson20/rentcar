﻿using Datos.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using Negocio.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentCar.Views
{
    public partial class Create_clientView : MaterialForm
    {
        public bool btnUpdate = false;
        public int idEmployee = 0;
        public Create_clientView(Client client = null)
        {
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            InitializeComponent();
            this.MaximizeBox = false;
            this.MaximizeBox = false;
            if (client != null)
            {
                name.Text = client.name;
                flastname.Text = client.firstLastName;
                slastname.Text = client.secondLastName;
                citizen.Text = client.citizenId;
                persontype.Text = client.personType;
                state.Text = client.state;
                limitcredit.Text = client.creditLimit.ToString();

                btnUpdate = true;
                idEmployee = client.clientId;
            }
        }

        private void Create_clientView_Load(object sender, EventArgs e)
        {
            persontype.DropDownStyle = ComboBoxStyle.DropDownList;
            state.DropDownStyle = ComboBoxStyle.DropDownList;
            state.SelectedIndex = 0;
            persontype.SelectedIndex = 0;
        }

        private void materialRaisedButton2_Click(object sender, EventArgs e)
        {
            ClientController controller = new ClientController();
            Client client = new Client();


            if (btnUpdate)
            {
                UpdateClient(client, controller, idEmployee);
            }
            else
            {
                saveClient(client, controller);
            }
            ClientView clientview = new ClientView();
            clientview.ShowDialog();
        }

        public void saveClient(Client client, ClientController controller)
        {
            client.name = name.Text;
            client.firstLastName = flastname.Text;
            client.secondLastName = slastname.Text;
            client.citizenId = citizen.Text;
            client.creditCarNumber = CreditCard.Text;
            client.personType = persontype.SelectedItem.ToString();
            client.state = state.SelectedItem.ToString();
            client.creditLimit = Int32.Parse(limitcredit.Text);


            int result = controller.CreateClient(client);
            if (result > 0)
            {
                MessageBox.Show("El Cliente se agrego correctamente", "Se guardo correctamente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("El Cliente no se pudo agregar",
                                "Error al guardar los datos",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

            }
            this.Hide();
        }

        public void UpdateClient(Client client, ClientController controller, int id)
        {

            client.name = name.Text;
            client.firstLastName = flastname.Text;
            client.secondLastName = slastname.Text;
            client.citizenId = citizen.Text;
            client.creditCarNumber = CreditCard.Text;
            client.personType = persontype.SelectedItem.ToString();
            client.state = state.SelectedItem.ToString();
            client.creditLimit = Int32.Parse(limitcredit.Text);
            int result = controller.UpdateClient(client, id);

            if (result > 0)
            {
                MessageBox.Show("El Cliente se actualizo correctamente", "Se Aactualizo correctamente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("El Cliente no se pudo actualizar",
                                "Error al Actualizar los datos",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

            }
            this.Hide();

        }

        private void persontype_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void limitcredit_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Verify that the pressed key isn't CTRL or any non-numeric digit
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // If you want, you can allow decimal (float) numbers
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void name_MouseClick(object sender, MouseEventArgs e)
        {
            name.Text = "";
        }

        private void flastname_MouseClick(object sender, MouseEventArgs e)
        {
            flastname.Text = "";
        }

        private void slastname_MouseClick(object sender, MouseEventArgs e)
        {
            slastname.Text = "";
        }

        private void limitcredit_MouseClick(object sender, MouseEventArgs e)
        {
            limitcredit.Text = "";
        }

        private void CreditCard_MouseClick(object sender, MouseEventArgs e)
        {
            CreditCard.Text = "";
        }

        private void CreditCard_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Verify that the pressed key isn't CTRL or any non-numeric digit
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // If you want, you can allow decimal (float) numbers
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void limitcredit_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
    }
}
