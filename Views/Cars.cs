﻿using Datos.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using RentCar.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentCar.Views
{
    public partial class Cars : MaterialForm
    {
       
        public Cars()
        {
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            InitializeComponent();
            this.MaximizeBox = false;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            
            var create_cars = new Create_Cars();
            create_cars.ShowDialog();
            setDataGrid();
        }

        private void Cars_Load(object sender, EventArgs e)
        {
            setDataGrid();
            filter.DropDownStyle = ComboBoxStyle.DropDownList;
            filter.Text = "Filtrar por:";
            seach.Enabled = false;
        }

        public void setDataGrid()
        {
            CarController cars = new CarController();
            List<Car> car = cars.getCars();
            var dt = cars.ConvertToDataTable(car);
            dataGridView1.DataSource = dt;

        }


      

        private void materialRaisedButton3_Click(object sender, EventArgs e)
        {
            CarController car = new CarController();
            int id = Int32.Parse(dataGridView1.CurrentRow.Cells[0].Value.ToString());
            int result = car.DeleteCar(id);
            if (result > 0)
            {
                MessageBox.Show("El vehiculo se borro correctamente", "Se borro correctamente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("El vehiculo no se pudo borrar",
                                "Error al borrar los datos",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

            }
            setDataGrid();
        }

        private void materialRaisedButton2_Click(object sender, EventArgs e)
        {
            CarController car = new CarController();
            

            int id = Int32.Parse( dataGridView1.CurrentRow.Cells[0].Value.ToString());
            var carEdit = car.getCarById(id);
            this.Hide();
            var ccars = new Create_Cars(carEdit);
            ccars.ShowDialog();
            

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
            
        }

        private void seach_TextChanged(object sender, EventArgs e)
        {
            CarController cars = new CarController();
            List<Car> car = cars.getCarsFilter(filter.SelectedItem.ToString(),seach.Text);

           
        
            var dt = cars.ConvertToDataTable(car);
            dataGridView1.DataSource = dt;
            
        }

        private void seach_MouseClick(object sender, MouseEventArgs e)
        {
            seach.Text = "";
        }

        private void filter_SelectedIndexChanged(object sender, EventArgs e)
        {
            seach.Enabled = true;
            if (filter.SelectedItem.ToString() == "descripcion")
            {
                seach.Text = "buscar por descripcion";
            }
            else
            {
                seach.Text = "buscar por numero de Chasis";
            }
        }

        private void filter_KeyDown(object sender, KeyEventArgs e)
        {
            
        }
    }
}
