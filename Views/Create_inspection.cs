﻿using Datos.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using Negocio.Controllers;
using RentCar.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentCar.Views
{
    public partial class Create_inspection : MaterialForm
    {
        Rent _rent;
       
        public Create_inspection(Rent rent)
        {
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            InitializeComponent();
            this.MaximizeBox = false;
            this._rent = rent;
            
        }

        private void Create_inspection_Load(object sender, EventArgs e)
        {
            EmployeeController ec = new EmployeeController();
            var employee = ec.getEmployeeById(_rent.employeeId);
            emplo.Text = employee.name + " " + employee.firstLastName;
                if (_rent.returned)
                {
                    type.Text = "Entrada";
                btnsalida.Visible = true;
                }
                else
                {
                    type.Text = "Salida";
                btnsalida.Visible = false;

                }
            amountOfFuel.SelectedIndex = 0;
            amountOfFuel.DropDownStyle = ComboBoxStyle.DropDownList;
            status.SelectedIndex = 0;
            status.DropDownStyle = ComboBoxStyle.DropDownList;
            emplo.Enabled = false;



        }

        public void saveInspection( InspectionController controller)
        {
            Inspection result = new Inspection();
            result.carId = _rent.carId;
           result.rentId = _rent.rentId;
            result.amountOfFuel = amountOfFuel.Text;
            result.employeeId = _rent.employeeId;
            result.date = DateTime.Today;
            result.hasCrystalCrashes = hasCrystalCrashes.Checked;
            result.hasHydraulicJack = hasHydraulicJack.Checked;
            result.hasResponseTire = hasResponseTire.Checked;
            result.hasScratches = hasScratches.Checked;
            result.InspectionType = type.Text;
            result.status = status.SelectedItem.ToString();
            result.tyreBacktLeft = tyreBacktLeft.Checked;
            result.tyreBacktRigth = tyreBacktRigth.Checked;
            result.tyreFrontLeft = tyreFrontLeft.Checked;
            result.tyreFrontRigth = tyreFrontRight.Checked;


            int results = controller.CreateInspection(result);
            if (results > 0)
            {
                MessageBox.Show("La inspeccion se agrego correctamente", "Se guardo correctamente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("La inspeccion no se pudo agregar",
                                "Error al guardar los datos",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

            }
            this.Hide();
        }

        public void UpdateInspection(InspectionController controller, int id)
        {

            
            Inspection result = new Inspection();
           result.rentId = _rent.rentId;
            result.amountOfFuel = amountOfFuel.Text;
            result.employeeId = _rent.employeeId;
            result.date = DateTime.Today;
            result.hasCrystalCrashes = hasCrystalCrashes.Checked;
            result.hasHydraulicJack = hasHydraulicJack.Checked;
            result.hasResponseTire = hasResponseTire.Checked;
            result.hasScratches = hasScratches.Checked;
            result.InspectionType = type.Text;
            result.status = status.SelectedItem.ToString();
            result.tyreBacktLeft = tyreBacktLeft.Checked;
            result.tyreBacktRigth = tyreBacktRigth.Checked;
            result.tyreFrontLeft = tyreFrontLeft.Checked;
            result.tyreFrontRigth = tyreFrontRight.Checked;

            int results = controller.UpdateInspection(result, id);
            if (results > 0)
            {
                MessageBox.Show("La inspeccion se actualizo correctamente", "Se Aactualizo correctamente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("La Inspeccion no se pudo actualizar",
                                "Error al Actualizar los datos",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

            }
            this.Hide();

        }

        public int saveRent(Rent rent, RentController controller)
        {
          

            int result = controller.CreateRent(rent);
            if (result > 0)
            {
                MessageBox.Show("La Renta se agrego correctamente", "Se guardo correctamente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("La Renta no se pudo agregar",
                                "Error al guardar los datos",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

            }
            this.Hide();
            return result;
            
        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            RentController rentcontroller = new RentController();
            EmployeeController emplo = new EmployeeController();
            CarController car = new CarController();
            InspectionController inspections = new InspectionController();

          
            var result = _rent.returned ? rentcontroller.UpdateRentRetun(_rent, _rent.rentId) : saveRent(_rent, rentcontroller);
            if (result>0)
            {
                var cars = car.getCarById(_rent.carId);
                if (_rent.returned)
                {
                    cars.state = "Activo";
                    var dataInit = _rent.dateReturn;
                    var mount = _rent.costByDay;
                    var today = DateTime.Today;
                    var craches = Craches();
                    var days = _rent.days;
                    var totalDays = mount * days;
                    var ExtraDays = ( today.Date - dataInit.Date).TotalDays;

                   
                    if(ExtraDays> days)
                    {
                        ExtraDays = ExtraDays - days;
                        ExtraDays = ExtraDays * (mount/2);
                    }
                    var Total = totalDays + ExtraDays + craches;
                    _rent.total = (float)Total;
                    rentcontroller.UpdateRentRetun(_rent, _rent.rentId);
                    MessageBox.Show("Total por dia: "+totalDays+"\n"+"Extra por pasarse de fecha: "+ExtraDays+"\nExtra por Daños: "+craches+" \n ------- Total: "+ Total + "------- \nDesea cargar el monto a la tarjeta del cliente? ", "Resumen de venta", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

                }
                else
                {
                    cars.state = "Inactivo";
                }
                
                car.UpdateCar(cars, _rent.carId);
                saveInspection(inspections);

                Employee employees = emplo.getEmployeeById(_rent.employeeId);
                RentView rentV = new RentView(employees);
                this.Hide();
                rentV.ShowDialog();
            }
        }

        private void btnsalida_Click(object sender, EventArgs e)
        {
            InspectionController inspections = new InspectionController();
            var i =inspections.InspectionsByRent(_rent.rentId);
            InspectionOut o = new InspectionOut(i);
            o.ShowDialog();

        }


        public int Craches()
        {
            Inspection i = new Inspection();
            i.hasCrystalCrashes = hasCrystalCrashes.Checked;
            i.hasHydraulicJack = hasHydraulicJack.Checked;
            i.hasResponseTire = hasResponseTire.Checked;
            i.hasScratches = hasScratches.Checked;
            i.tyreBacktLeft = tyreBacktLeft.Checked;
            i.tyreBacktRigth = tyreBacktRigth.Checked;
            i.tyreFrontLeft = tyreFrontLeft.Checked;
            i.tyreFrontRigth = tyreFrontRight.Checked;


            var c = 0;
            c += i.hasCrystalCrashes ? 1000 : 0;
            c += i.hasHydraulicJack ? 0 : 1000;
            c += i.hasResponseTire ? 0 : 1000;
            c += i.hasScratches ? 1000 : 0;
            c += i.tyreBacktLeft ? 0 : 1000;
            c += i.tyreBacktRigth ? 0 : 1000;
            c += i.tyreFrontLeft ? 0 : 1000;
            c += i.tyreFrontRigth ? 0 : 1000;



            return c;

        }


    }
}
