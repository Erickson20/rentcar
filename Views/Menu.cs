﻿using Datos.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using RentCar.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentCar
{
    public partial class Menu : MaterialForm
    {
        Employee _user;
        
        public Menu(Employee user  = null )
        {
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            InitializeComponent();
           
            
            this._user = user;
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            EmployeeView emplo = new EmployeeView();
            //this.Hide();
            emplo.ShowDialog();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Menu_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Cars cars = new Cars();
            //this.Hide();
            cars.Show();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            ClientView client = new ClientView();
            client.ShowDialog();

        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            settings s = new settings();
            s.ShowDialog();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            RentView rent = new RentView(_user);
            rent.ShowDialog();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            ReportView rep = new ReportView();
            rep.ShowDialog();
        }
    }
}
