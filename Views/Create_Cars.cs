﻿using MaterialSkin;
using MaterialSkin.Controls;
using System;
using Datos.Models;
using RentCar.Controllers;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Datos;

namespace RentCar.Views
{
    public partial class Create_Cars : MaterialForm
    {
        public bool btnUpdate = false;
        public int idCar = 0;
        public Car _car;
        public Create_Cars(Car car = null)
        {
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            InitializeComponent();
            this.MaximizeBox = false;
            
            if(car != null)
            {
                
                CarTypesController ct = new CarTypesController();
                ModelsController mc = new ModelsController();
                FuelTypesController fc = new FuelTypesController();
                BrandController bc = new BrandController();

                motor.Text = car.motorNumber.ToString();
                placa.Text = car.plateNumber.ToString();
                chasis.Text = car.chasisNumber.ToString();
                car.model = mc.getModelById(car.modelId);
                descriptions.Text = car.description;
                btnUpdate = true;
                idCar = car.carId;
                this._car = car;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            
        
        }

        private void textBox1_MouseClick(object sender, MouseEventArgs e)
        {
            motor.Text = "";
        }

        private void textBox3_Click(object sender, EventArgs e)
        {
            placa.Text = "";
        }

        private void textBox2_Click(object sender, EventArgs e)
        {
            chasis.Text = "";
        }

        private void comboBox2_Click(object sender, EventArgs e)
        {
            Brand.Text = "";
        }

        private void comboBox3_Click(object sender, EventArgs e)
        {
            models.Text = "";
        }

        private void comboBox1_Click(object sender, EventArgs e)
        {
            carType.Text = "";
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox4_Click(object sender, EventArgs e)
        {
            fuelType.Text = "";
        }

        private void comboBox5_Click(object sender, EventArgs e)
        {
            state.Text = "";
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox1_Click(object sender, EventArgs e)
        {
            descriptions.Text = "";
        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            CarController controller = new CarController();
            Car car = new Car();

            if (btnUpdate)
            {
                 UpdateCar(car,  controller, idCar);
            }
            else
            {
                saveCar(car, controller);
            }
            Cars cars = new Cars();
            cars.ShowDialog();
            /*
            Brand brand;
            using (RentCarContext ctx = new RentCarContext())
            {
                brand = ctx.Brands.SingleOrDefault(x=> x.description)

            }
            */
           
            
        }


        public void saveCar(Car car, CarController controller)
        {
            car.chasisNumber = Int32.Parse(chasis.Text);
            car.description = descriptions.Text;
            car.motorNumber = Int32.Parse(motor.Text);
            car.plateNumber = Int32.Parse(placa.Text);
            car.carTypeId = Int32.Parse(carType.SelectedValue.ToString());
            car.state = state.SelectedItem.ToString();
            car.modelId = Int32.Parse(models.SelectedValue.ToString());
            car.fuelTypeId = Int32.Parse(fuelType.SelectedValue.ToString());

            int result = controller.CreateCar(car);
            if (result > 0)
            {
                MessageBox.Show("El vehiculo se agrego correctamente", "Se guardo correctamente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("El vehiculo no se pudo agregar",
                                "Error al guardar los datos",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

            }
            this.Hide();
        }

        public void UpdateCar(Car car, CarController controller, int id)
        {
            car.chasisNumber = Int32.Parse(chasis.Text);
            car.description = descriptions.Text;
            car.motorNumber = Int32.Parse(motor.Text);
            car.plateNumber = Int32.Parse(placa.Text);
            car.carTypeId = Int32.Parse(carType.SelectedValue.ToString());
            car.state = state.SelectedItem.ToString();
            car.modelId = Int32.Parse(models.SelectedValue.ToString());
            car.fuelTypeId = Int32.Parse(fuelType.SelectedValue.ToString());

            int result = controller.UpdateCar(car, id);

            if (result > 0)
            {
                MessageBox.Show("El vehiculo se actualizo correctamente", "Se Aactualizo correctamente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("El vehiculo no se pudo actualizar",
                                "Error al Actualizar los datos",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

            }
            this.Hide();

        }

        private void Create_Cars_Load(object sender, EventArgs e)
        {
            FuelTypesController fuel = new FuelTypesController();
            BrandController brandc = new BrandController();
            ModelsController mc = new ModelsController();
            CarTypesController car = new CarTypesController();
            List<Brand> brands = brandc.getBrands();
            List<FuelType> fuelTypes = fuel.getFuelTypes();

            List<CarType> carTypes = car.getCarTypes();

            fuelType.DropDownStyle = ComboBoxStyle.DropDownList;
            carType.DropDownStyle = ComboBoxStyle.DropDownList;
            Brand.DropDownStyle = ComboBoxStyle.DropDownList;
            models.DropDownStyle = ComboBoxStyle.DropDownList;
            state.DropDownStyle = ComboBoxStyle.DropDownList;

            fuelType.DataSource = fuelTypes;
            fuelType.ValueMember = "fuelTypeId";
            fuelType.DisplayMember = "description";


            carType.DataSource = carTypes;
            carType.ValueMember = "carTypeId";
            carType.DisplayMember = "description";


            Brand.DataSource = brands;
            Brand.ValueMember = "brandId";
            Brand.DisplayMember = "description";


            models.ValueMember = "modelId";
            models.DisplayMember = "description";
            

            if (idCar > 0)
            {
                fuelType.SelectedValue = _car.fuelTypeId;
                carType.SelectedValue = _car.carTypeId;
                Brand.SelectedValue = _car.model.brandId;
                state.SelectedItem = _car.state;
                fuelType.SelectedValue = _car.fuelTypeId;
                models.SelectedValue = _car.modelId;

            }

        }

        private void Brand_SelectedIndexChanged(object sender, EventArgs e)
        {

          

        }

        private void Brand_DropDownClosed(object sender, EventArgs e)
        {
            ModelsController modelc = new ModelsController();
            if (Brand.SelectedValue != null)
            {
                int model_id = Int32.Parse(Brand.SelectedValue.ToString());
                List<Model> model = modelc.getModelByBrand(model_id);

                models.DataSource = model;
                models.ValueMember = "id";
                models.DisplayMember = "description";
            }
         
        }
    }
}
