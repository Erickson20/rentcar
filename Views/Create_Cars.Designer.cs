﻿namespace RentCar.Views
{
    partial class Create_Cars
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.carType = new System.Windows.Forms.ComboBox();
            this.motor = new System.Windows.Forms.TextBox();
            this.chasis = new System.Windows.Forms.TextBox();
            this.placa = new System.Windows.Forms.TextBox();
            this.Brand = new System.Windows.Forms.ComboBox();
            this.models = new System.Windows.Forms.ComboBox();
            this.fuelType = new System.Windows.Forms.ComboBox();
            this.state = new System.Windows.Forms.ComboBox();
            this.descriptions = new System.Windows.Forms.RichTextBox();
            this.materialRaisedButton1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.SuspendLayout();
            // 
            // carType
            // 
            this.carType.FormattingEnabled = true;
            this.carType.Location = new System.Drawing.Point(12, 102);
            this.carType.Name = "carType";
            this.carType.Size = new System.Drawing.Size(158, 21);
            this.carType.TabIndex = 0;
            this.carType.Text = "Tipo de vehiculo";
            this.carType.Click += new System.EventHandler(this.comboBox1_Click);
            // 
            // motor
            // 
            this.motor.Location = new System.Drawing.Point(207, 102);
            this.motor.Name = "motor";
            this.motor.Size = new System.Drawing.Size(165, 20);
            this.motor.TabIndex = 1;
            this.motor.Text = "No. Motor";
            this.motor.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBox1_MouseClick);
            this.motor.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // chasis
            // 
            this.chasis.Location = new System.Drawing.Point(12, 145);
            this.chasis.Name = "chasis";
            this.chasis.Size = new System.Drawing.Size(158, 20);
            this.chasis.TabIndex = 2;
            this.chasis.Text = "No. Chasis";
            this.chasis.Click += new System.EventHandler(this.textBox2_Click);
            // 
            // placa
            // 
            this.placa.Location = new System.Drawing.Point(207, 145);
            this.placa.Name = "placa";
            this.placa.Size = new System.Drawing.Size(165, 20);
            this.placa.TabIndex = 3;
            this.placa.Text = "No. Placa";
            this.placa.Click += new System.EventHandler(this.textBox3_Click);
            // 
            // Brand
            // 
            this.Brand.FormattingEnabled = true;
            this.Brand.Location = new System.Drawing.Point(12, 191);
            this.Brand.Name = "Brand";
            this.Brand.Size = new System.Drawing.Size(158, 21);
            this.Brand.TabIndex = 4;
            this.Brand.Text = "Marca";
            this.Brand.SelectedIndexChanged += new System.EventHandler(this.Brand_SelectedIndexChanged);
            this.Brand.DropDownClosed += new System.EventHandler(this.Brand_DropDownClosed);
            this.Brand.Click += new System.EventHandler(this.comboBox2_Click);
            // 
            // models
            // 
            this.models.FormattingEnabled = true;
            this.models.Location = new System.Drawing.Point(207, 191);
            this.models.Name = "models";
            this.models.Size = new System.Drawing.Size(165, 21);
            this.models.TabIndex = 5;
            this.models.Text = "Modelo";
            this.models.Click += new System.EventHandler(this.comboBox3_Click);
            // 
            // fuelType
            // 
            this.fuelType.FormattingEnabled = true;
            this.fuelType.Location = new System.Drawing.Point(12, 247);
            this.fuelType.Name = "fuelType";
            this.fuelType.Size = new System.Drawing.Size(158, 21);
            this.fuelType.TabIndex = 6;
            this.fuelType.Text = "Tipo de combustible";
            this.fuelType.SelectedIndexChanged += new System.EventHandler(this.comboBox4_SelectedIndexChanged);
            this.fuelType.Click += new System.EventHandler(this.comboBox4_Click);
            // 
            // state
            // 
            this.state.FormattingEnabled = true;
            this.state.Items.AddRange(new object[] {
            "Activo",
            "Inactivo"});
            this.state.Location = new System.Drawing.Point(207, 247);
            this.state.Name = "state";
            this.state.Size = new System.Drawing.Size(165, 21);
            this.state.TabIndex = 7;
            this.state.Text = "Estado";
            this.state.Click += new System.EventHandler(this.comboBox5_Click);
            // 
            // descriptions
            // 
            this.descriptions.Location = new System.Drawing.Point(12, 287);
            this.descriptions.Name = "descriptions";
            this.descriptions.Size = new System.Drawing.Size(360, 81);
            this.descriptions.TabIndex = 8;
            this.descriptions.Text = "Descripcion";
            this.descriptions.Click += new System.EventHandler(this.richTextBox1_Click);
            this.descriptions.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            // 
            // materialRaisedButton1
            // 
            this.materialRaisedButton1.Depth = 0;
            this.materialRaisedButton1.Location = new System.Drawing.Point(148, 379);
            this.materialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton1.Name = "materialRaisedButton1";
            this.materialRaisedButton1.Primary = true;
            this.materialRaisedButton1.Size = new System.Drawing.Size(75, 23);
            this.materialRaisedButton1.TabIndex = 9;
            this.materialRaisedButton1.Text = "Guardar";
            this.materialRaisedButton1.UseVisualStyleBackColor = true;
            this.materialRaisedButton1.Click += new System.EventHandler(this.materialRaisedButton1_Click);
            // 
            // Create_Cars
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(392, 414);
            this.Controls.Add(this.materialRaisedButton1);
            this.Controls.Add(this.descriptions);
            this.Controls.Add(this.state);
            this.Controls.Add(this.fuelType);
            this.Controls.Add(this.models);
            this.Controls.Add(this.Brand);
            this.Controls.Add(this.placa);
            this.Controls.Add(this.chasis);
            this.Controls.Add(this.motor);
            this.Controls.Add(this.carType);
            this.Name = "Create_Cars";
            this.Text = "Create_Cars";
            this.Load += new System.EventHandler(this.Create_Cars_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox carType;
        private System.Windows.Forms.TextBox motor;
        private System.Windows.Forms.TextBox chasis;
        private System.Windows.Forms.TextBox placa;
        private System.Windows.Forms.ComboBox Brand;
        private System.Windows.Forms.ComboBox models;
        private System.Windows.Forms.ComboBox fuelType;
        private System.Windows.Forms.ComboBox state;
        private System.Windows.Forms.RichTextBox descriptions;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton1;
    }
}