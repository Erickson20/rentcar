﻿using Datos.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using RentCar.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentCar.Views
{
    public partial class settings : MaterialForm
    {
        public settings()
        {
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            InitializeComponent();
            this.MaximizeBox = false;
        }

        private void settings_Load(object sender, EventArgs e)
        {
            setDataGrid();
            filter.SelectedIndex = 0;
            filter.DropDownStyle = ComboBoxStyle.DropDownList;
            seach.Enabled = false;
        }
        public void setDataGrid()
        {
            BrandController b = new BrandController();
            ModelsController m = new ModelsController();
            CarTypesController c = new CarTypesController();
            List<Model> models = m.getModels();
            List<Brand> brands = b.getBrands();
            List<CarType> carTypes = c.getCarTypes();
            brand.DataSource = brands.Select(o => new
            { Id = o.brandId, Descripcion = o.description }).ToList(); 

            model.DataSource = models.Select(o => new
            { Id = o.id, Descripcion = o.description }).ToList(); 

            cartType.DataSource = carTypes.Select(o => new
            { Id = o.carTypeId, Descripcion = o.description }).ToList();

        }

        private void dataGridView3_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            create_settings settings = new create_settings("brand");
            settings.ShowDialog();
        }

        private void brand_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void materialRaisedButton5_Click(object sender, EventArgs e)
        {
            create_settings settings = new create_settings("carType");
            settings.ShowDialog();
        }

        private void materialRaisedButton3_Click(object sender, EventArgs e)
        {
            create_settings settings = new create_settings("model");
            settings.ShowDialog();
        }

        private void materialRaisedButton2_Click(object sender, EventArgs e)
        {
            BrandController b = new BrandController();
            int id = (int)brand.CurrentRow.Cells[0].Value;
            int result = b.DeleteBrand(id);
            if (result > 0)
            {
                MessageBox.Show("La marca se borro correctamente", "Se borro correctamente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("La marca no se pudo borrar",
                                "Error al borrar los datos",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

            }
            setDataGrid();
        }

        private void materialRaisedButton4_Click(object sender, EventArgs e)
        {
            ModelsController m = new ModelsController();
            int id = (int)brand.CurrentRow.Cells[0].Value;
            int result = m.DeleteModel(id);
            if (result > 0)
            {
                MessageBox.Show("El modelo se borro correctamente", "Se borro correctamente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("El modelo no se pudo borrar",
                                "Error al borrar los datos",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

            }
            setDataGrid();
        }

        private void materialRaisedButton6_Click(object sender, EventArgs e)
        {
            CarTypesController c = new CarTypesController();
            int id = (int)cartType.CurrentRow.Cells[0].Value;
            int result = c.DeleteCarTypes(id);
            if (result > 0)
            {
                MessageBox.Show("El tipo de vehiculo se borro correctamente", "Se borro correctamente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("El tipo de vehiculo no se pudo borrar",
                                "Error al borrar los datos",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

            }
            setDataGrid();

        }

        private void filter_SelectedIndexChanged(object sender, EventArgs e)
        {
            seach.Enabled = true;
            if (filter.SelectedItem.ToString() == "Modelo")
            {
                seach.Text = "buscar modelo";
            }
            else if(filter.SelectedItem.ToString() == "Marca")
            {
                seach.Text = "buscar marca";
            }
            else if (filter.SelectedItem.ToString() == "Tipo de vehiculo")
            {
                seach.Text = "buscar tipo de vehiculo";
            }
            else
            {
                seach.Text = "";
            }
        }

        private void seach_TextChanged(object sender, EventArgs e)
        {
            
            if (filter.SelectedItem.ToString() == "Modelo")
            {
                ModelsController m = new ModelsController();
                List<Model> models = m.getModelFilter(seach.Text);
                model.DataSource = models;
            }
            else if (filter.SelectedItem.ToString() == "Marca")
            {
                BrandController b = new BrandController();
                List<Brand> brands = b.getBrandsFilter(seach.Text);
                brand.DataSource = brands;
            }
            else if (filter.SelectedItem.ToString() == "Tipo de vehiculo")
            {
                CarTypesController carTypes = new CarTypesController();
                List<CarType> c = carTypes.getTypesFilter(seach.Text);
                cartType.DataSource = c;
            }
            else
            {
                
            }
            



            
        }

        private void seach_MouseClick(object sender, MouseEventArgs e)
        {
            seach.Text = "";
        }
    }
}
