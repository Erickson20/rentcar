﻿using Datos.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentCar.Views
{
    public partial class InspectionOut : MaterialForm
    {
        Inspection inspection;
        public InspectionOut(Inspection i)
        {
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            InitializeComponent();
            this.inspection = i;
        }

        private void InspectionOut_Load(object sender, EventArgs e)
        {
            hasCrystalCrashes.Checked = inspection.hasCrystalCrashes? true: false;
            hasHydraulicJack.Checked = inspection.hasHydraulicJack ? true : false;
            hasResponseTire.Checked = inspection.hasResponseTire ? true : false;
            hasScratches.Checked = inspection.hasScratches ? true : false;
            amount.Text = inspection.amountOfFuel;
            tyreBacktLeft.Checked = inspection.tyreBacktLeft;
            tyreBacktRigth.Checked = inspection.tyreBacktRigth;
            tyreFrontLeft.Checked = inspection.tyreFrontLeft;
            tyreFrontRight.Checked = inspection.tyreFrontRigth;
            amount.Enabled = false;

        }
    }
}
