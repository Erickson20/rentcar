﻿namespace RentCar.Views
{
    partial class Create_inspection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.hasScratches = new MaterialSkin.Controls.MaterialCheckBox();
            this.amountOfFuel = new System.Windows.Forms.ComboBox();
            this.hasResponseTire = new MaterialSkin.Controls.MaterialCheckBox();
            this.hasHydraulicJack = new MaterialSkin.Controls.MaterialCheckBox();
            this.hasCrystalCrashes = new MaterialSkin.Controls.MaterialCheckBox();
            this.tyreFrontRight = new MaterialSkin.Controls.MaterialCheckBox();
            this.tyreFrontLeft = new MaterialSkin.Controls.MaterialCheckBox();
            this.tyreBacktRigth = new MaterialSkin.Controls.MaterialCheckBox();
            this.tyreBacktLeft = new MaterialSkin.Controls.MaterialCheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.status = new System.Windows.Forms.ComboBox();
            this.emplo = new System.Windows.Forms.TextBox();
            this.materialRaisedButton1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.label1 = new System.Windows.Forms.Label();
            this.type = new System.Windows.Forms.Label();
            this.btnsalida = new MaterialSkin.Controls.MaterialRaisedButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // hasScratches
            // 
            this.hasScratches.AutoSize = true;
            this.hasScratches.Depth = 0;
            this.hasScratches.Font = new System.Drawing.Font("Roboto", 10F);
            this.hasScratches.Location = new System.Drawing.Point(9, 110);
            this.hasScratches.Margin = new System.Windows.Forms.Padding(0);
            this.hasScratches.MouseLocation = new System.Drawing.Point(-1, -1);
            this.hasScratches.MouseState = MaterialSkin.MouseState.HOVER;
            this.hasScratches.Name = "hasScratches";
            this.hasScratches.Ripple = true;
            this.hasScratches.Size = new System.Drawing.Size(129, 30);
            this.hasScratches.TabIndex = 1;
            this.hasScratches.Text = "Tiene rayaduras";
            this.hasScratches.UseVisualStyleBackColor = true;
            // 
            // amountOfFuel
            // 
            this.amountOfFuel.FormattingEnabled = true;
            this.amountOfFuel.Items.AddRange(new object[] {
            "1/4",
            "1/2",
            "1/3",
            "lleno"});
            this.amountOfFuel.Location = new System.Drawing.Point(12, 374);
            this.amountOfFuel.Name = "amountOfFuel";
            this.amountOfFuel.Size = new System.Drawing.Size(172, 21);
            this.amountOfFuel.TabIndex = 2;
            this.amountOfFuel.Text = "Cantida de Combustible";
            // 
            // hasResponseTire
            // 
            this.hasResponseTire.AutoSize = true;
            this.hasResponseTire.Depth = 0;
            this.hasResponseTire.Font = new System.Drawing.Font("Roboto", 10F);
            this.hasResponseTire.Location = new System.Drawing.Point(9, 174);
            this.hasResponseTire.Margin = new System.Windows.Forms.Padding(0);
            this.hasResponseTire.MouseLocation = new System.Drawing.Point(-1, -1);
            this.hasResponseTire.MouseState = MaterialSkin.MouseState.HOVER;
            this.hasResponseTire.Name = "hasResponseTire";
            this.hasResponseTire.Ripple = true;
            this.hasResponseTire.Size = new System.Drawing.Size(162, 30);
            this.hasResponseTire.TabIndex = 3;
            this.hasResponseTire.Text = "Tiene Goma repuesta";
            this.hasResponseTire.UseVisualStyleBackColor = true;
            // 
            // hasHydraulicJack
            // 
            this.hasHydraulicJack.AutoSize = true;
            this.hasHydraulicJack.Depth = 0;
            this.hasHydraulicJack.Font = new System.Drawing.Font("Roboto", 10F);
            this.hasHydraulicJack.Location = new System.Drawing.Point(202, 174);
            this.hasHydraulicJack.Margin = new System.Windows.Forms.Padding(0);
            this.hasHydraulicJack.MouseLocation = new System.Drawing.Point(-1, -1);
            this.hasHydraulicJack.MouseState = MaterialSkin.MouseState.HOVER;
            this.hasHydraulicJack.Name = "hasHydraulicJack";
            this.hasHydraulicJack.Ripple = true;
            this.hasHydraulicJack.Size = new System.Drawing.Size(97, 30);
            this.hasHydraulicJack.TabIndex = 4;
            this.hasHydraulicJack.Text = "Tiene Gato";
            this.hasHydraulicJack.UseVisualStyleBackColor = true;
            // 
            // hasCrystalCrashes
            // 
            this.hasCrystalCrashes.AutoSize = true;
            this.hasCrystalCrashes.Depth = 0;
            this.hasCrystalCrashes.Font = new System.Drawing.Font("Roboto", 10F);
            this.hasCrystalCrashes.Location = new System.Drawing.Point(202, 110);
            this.hasCrystalCrashes.Margin = new System.Windows.Forms.Padding(0);
            this.hasCrystalCrashes.MouseLocation = new System.Drawing.Point(-1, -1);
            this.hasCrystalCrashes.MouseState = MaterialSkin.MouseState.HOVER;
            this.hasCrystalCrashes.Name = "hasCrystalCrashes";
            this.hasCrystalCrashes.Ripple = true;
            this.hasCrystalCrashes.Size = new System.Drawing.Size(172, 30);
            this.hasCrystalCrashes.TabIndex = 5;
            this.hasCrystalCrashes.Text = "Tiene roturas de cristal";
            this.hasCrystalCrashes.UseVisualStyleBackColor = true;
            // 
            // tyreFrontRight
            // 
            this.tyreFrontRight.AutoSize = true;
            this.tyreFrontRight.Depth = 0;
            this.tyreFrontRight.Font = new System.Drawing.Font("Roboto", 10F);
            this.tyreFrontRight.Location = new System.Drawing.Point(3, 27);
            this.tyreFrontRight.Margin = new System.Windows.Forms.Padding(0);
            this.tyreFrontRight.MouseLocation = new System.Drawing.Point(-1, -1);
            this.tyreFrontRight.MouseState = MaterialSkin.MouseState.HOVER;
            this.tyreFrontRight.Name = "tyreFrontRight";
            this.tyreFrontRight.Ripple = true;
            this.tyreFrontRight.Size = new System.Drawing.Size(143, 30);
            this.tyreFrontRight.TabIndex = 6;
            this.tyreFrontRight.Text = "Goma d. delantera";
            this.tyreFrontRight.UseVisualStyleBackColor = true;
            // 
            // tyreFrontLeft
            // 
            this.tyreFrontLeft.AutoSize = true;
            this.tyreFrontLeft.Depth = 0;
            this.tyreFrontLeft.Font = new System.Drawing.Font("Roboto", 10F);
            this.tyreFrontLeft.Location = new System.Drawing.Point(193, 27);
            this.tyreFrontLeft.Margin = new System.Windows.Forms.Padding(0);
            this.tyreFrontLeft.MouseLocation = new System.Drawing.Point(-1, -1);
            this.tyreFrontLeft.MouseState = MaterialSkin.MouseState.HOVER;
            this.tyreFrontLeft.Name = "tyreFrontLeft";
            this.tyreFrontLeft.Ripple = true;
            this.tyreFrontLeft.Size = new System.Drawing.Size(139, 30);
            this.tyreFrontLeft.TabIndex = 7;
            this.tyreFrontLeft.Text = "Goma i. delantera";
            this.tyreFrontLeft.UseVisualStyleBackColor = true;
            // 
            // tyreBacktRigth
            // 
            this.tyreBacktRigth.AutoSize = true;
            this.tyreBacktRigth.Depth = 0;
            this.tyreBacktRigth.Font = new System.Drawing.Font("Roboto", 10F);
            this.tyreBacktRigth.Location = new System.Drawing.Point(12, 291);
            this.tyreBacktRigth.Margin = new System.Windows.Forms.Padding(0);
            this.tyreBacktRigth.MouseLocation = new System.Drawing.Point(-1, -1);
            this.tyreBacktRigth.MouseState = MaterialSkin.MouseState.HOVER;
            this.tyreBacktRigth.Name = "tyreBacktRigth";
            this.tyreBacktRigth.Ripple = true;
            this.tyreBacktRigth.Size = new System.Drawing.Size(128, 30);
            this.tyreBacktRigth.TabIndex = 8;
            this.tyreBacktRigth.Text = "Goma d. trasera";
            this.tyreBacktRigth.UseVisualStyleBackColor = true;
            // 
            // tyreBacktLeft
            // 
            this.tyreBacktLeft.AutoSize = true;
            this.tyreBacktLeft.Depth = 0;
            this.tyreBacktLeft.Font = new System.Drawing.Font("Roboto", 10F);
            this.tyreBacktLeft.Location = new System.Drawing.Point(202, 291);
            this.tyreBacktLeft.Margin = new System.Windows.Forms.Padding(0);
            this.tyreBacktLeft.MouseLocation = new System.Drawing.Point(-1, -1);
            this.tyreBacktLeft.MouseState = MaterialSkin.MouseState.HOVER;
            this.tyreBacktLeft.Name = "tyreBacktLeft";
            this.tyreBacktLeft.Ripple = true;
            this.tyreBacktLeft.Size = new System.Drawing.Size(124, 30);
            this.tyreBacktLeft.TabIndex = 9;
            this.tyreBacktLeft.Text = "Goma i. trasera";
            this.tyreBacktLeft.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tyreFrontRight);
            this.groupBox1.Controls.Add(this.tyreFrontLeft);
            this.groupBox1.Location = new System.Drawing.Point(9, 221);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(365, 132);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Estado de gomas";
            // 
            // status
            // 
            this.status.FormattingEnabled = true;
            this.status.Items.AddRange(new object[] {
            "Activo",
            "Inactivo"});
            this.status.Location = new System.Drawing.Point(202, 374);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(172, 21);
            this.status.TabIndex = 11;
            this.status.Text = "Estado";
            // 
            // emplo
            // 
            this.emplo.Location = new System.Drawing.Point(13, 419);
            this.emplo.Name = "emplo";
            this.emplo.Size = new System.Drawing.Size(361, 20);
            this.emplo.TabIndex = 12;
            this.emplo.Text = "Empleado";
            // 
            // materialRaisedButton1
            // 
            this.materialRaisedButton1.Depth = 0;
            this.materialRaisedButton1.Location = new System.Drawing.Point(107, 456);
            this.materialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton1.Name = "materialRaisedButton1";
            this.materialRaisedButton1.Primary = true;
            this.materialRaisedButton1.Size = new System.Drawing.Size(166, 23);
            this.materialRaisedButton1.TabIndex = 13;
            this.materialRaisedButton1.Text = "Guardar Inspeccion";
            this.materialRaisedButton1.UseVisualStyleBackColor = true;
            this.materialRaisedButton1.Click += new System.EventHandler(this.materialRaisedButton1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 16);
            this.label1.TabIndex = 14;
            this.label1.Text = "Tipo:";
            // 
            // type
            // 
            this.type.AutoSize = true;
            this.type.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.type.Location = new System.Drawing.Point(57, 77);
            this.type.Name = "type";
            this.type.Size = new System.Drawing.Size(0, 16);
            this.type.TabIndex = 15;
            // 
            // btnsalida
            // 
            this.btnsalida.Depth = 0;
            this.btnsalida.Location = new System.Drawing.Point(74, 485);
            this.btnsalida.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnsalida.Name = "btnsalida";
            this.btnsalida.Primary = true;
            this.btnsalida.Size = new System.Drawing.Size(225, 23);
            this.btnsalida.TabIndex = 16;
            this.btnsalida.Text = "Ver Inspeccion de salida";
            this.btnsalida.UseVisualStyleBackColor = true;
            this.btnsalida.Click += new System.EventHandler(this.btnsalida_Click);
            // 
            // Create_inspection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(395, 532);
            this.Controls.Add(this.btnsalida);
            this.Controls.Add(this.type);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.materialRaisedButton1);
            this.Controls.Add(this.emplo);
            this.Controls.Add(this.status);
            this.Controls.Add(this.tyreBacktLeft);
            this.Controls.Add(this.tyreBacktRigth);
            this.Controls.Add(this.hasCrystalCrashes);
            this.Controls.Add(this.hasHydraulicJack);
            this.Controls.Add(this.hasResponseTire);
            this.Controls.Add(this.amountOfFuel);
            this.Controls.Add(this.hasScratches);
            this.Controls.Add(this.groupBox1);
            this.Name = "Create_inspection";
            this.Text = "Create_inspection";
            this.Load += new System.EventHandler(this.Create_inspection_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private MaterialSkin.Controls.MaterialCheckBox hasScratches;
        private System.Windows.Forms.ComboBox amountOfFuel;
        private MaterialSkin.Controls.MaterialCheckBox hasResponseTire;
        private MaterialSkin.Controls.MaterialCheckBox hasHydraulicJack;
        private MaterialSkin.Controls.MaterialCheckBox hasCrystalCrashes;
        private MaterialSkin.Controls.MaterialCheckBox tyreFrontRight;
        private MaterialSkin.Controls.MaterialCheckBox tyreFrontLeft;
        private MaterialSkin.Controls.MaterialCheckBox tyreBacktRigth;
        private MaterialSkin.Controls.MaterialCheckBox tyreBacktLeft;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox status;
        private System.Windows.Forms.TextBox emplo;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label type;
        private MaterialSkin.Controls.MaterialRaisedButton btnsalida;
    }
}