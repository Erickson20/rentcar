﻿using Datos.Models;
using MaterialSkin;
using MaterialSkin.Controls;
using Negocio.Controllers;
using RentCar.Controllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentCar.Views
{
    public partial class create_rent : MaterialForm
    {
        public bool btnUpdate = false;
        public int id;
        public Employee _user;
        public create_rent( Employee user)
        {
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE);
            InitializeComponent();
            this.MaximizeBox = false;
            this._user = user;
        }

        private void create_rent_Load(object sender, EventArgs e)
        {
            ClientController client = new ClientController();
            CarController cars = new CarController();
            var car = cars.getCarsActived();
            var c = client.GetClients();
            clientSelect.DataSource = c;
            clientSelect.ValueMember = "clientId";
            clientSelect.DisplayMember = "name";
            carSelect.DataSource = car;
            carSelect.ValueMember = "carId";
            carSelect.DisplayMember = "description";

            employee.Text = _user.name +" " + _user.firstLastName + " " + _user.secondLastName;
            employee.Enabled = false;

            carSelect.DropDownStyle = ComboBoxStyle.DropDownList;
            carSelect.SelectedIndex = 0;

            clientSelect.DropDownStyle = ComboBoxStyle.DropDownList;
            clientSelect.SelectedIndex = 0;

            state.DropDownStyle = ComboBoxStyle.DropDownList;
            state.SelectedIndex = 0;


        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            RentController rentcontroller = new RentController();
            Rent rent = new Rent();


            rent.employeeId = _user.employeeId;
            rent.carId = Int32.Parse(carSelect.SelectedValue.ToString());
            rent.clientId = Int32.Parse(clientSelect.SelectedValue.ToString());
            var ticks = new DateTime(2016, 1, 1).Ticks;
            var ans = DateTime.Now.Ticks - ticks;
            var uniqueId = ans.ToString("x");

            rent.comment = comment.Text;
            rent.returned = false;
            rent.status = state.SelectedItem.ToString();
            rent.rentNumber = 1;
            rent.costByDay = Int32.Parse(payfordays.Text);
            rent.days = Int32.Parse(days.Text);
            rent.dateReturn = dateRetorned.Value;
            rent.dateRent = DateTime.Today;

            Create_inspection inspection = new Create_inspection(rent);
            inspection.ShowDialog();
               
            
            this.Close();
        }

        public void saveRent(Rent rent, RentController controller)
        {
          





            int result = controller.CreateRent(rent);
            if (result > 0)
            {
                MessageBox.Show("La Renta se agrego correctamente", "Se guardo correctamente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("La Renta no se pudo agregar",
                                "Error al guardar los datos",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

            }
            this.Hide();
        }

        private void days_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Verify that the pressed key isn't CTRL or any non-numeric digit
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // If you want, you can allow decimal (float) numbers
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void payfordays_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Verify that the pressed key isn't CTRL or any non-numeric digit
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // If you want, you can allow decimal (float) numbers
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void comment_MouseClick(object sender, MouseEventArgs e)
        {
            comment.Text = "";
        }

        private void payfordays_MouseClick(object sender, MouseEventArgs e)
        {
            payfordays.Text = "";
        }

        private void days_MouseClick(object sender, MouseEventArgs e)
        {
            days.Text = "";
        }
    }
}
