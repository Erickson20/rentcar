﻿using Datos;
using Datos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentCar.Controllers
{
    public  class FuelTypesController
    {

        public List<FuelType> getFuelTypes()
        {
            List<FuelType> _types;
            using (RentCarContext ctx = new RentCarContext())
            {

                _types = ctx.FuelTypes.ToList();
            }

            return _types; 

        }

        public FuelType GetFuelTypesById(int id)
        {
            FuelType c;

            using (RentCarContext ctx = new RentCarContext())
            {
                c = ctx.FuelTypes.SingleOrDefault(x => x.fuelTypeId == id);
                return c;
            }

        }
    }
}
