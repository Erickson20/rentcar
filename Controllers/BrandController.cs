﻿using Datos;
using Datos.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentCar.Controllers
{
    public class BrandController
    {

        public List<Brand> getBrands()
        {
            List<Brand> _brands;
            using (RentCarContext ctx = new RentCarContext())
            {

                _brands = ctx.Brands.ToList();
            }

            return _brands;

        }

        public Brand getBrand(int id)
        {
            Brand _brand;
            using (RentCarContext ctx = new RentCarContext())
            {
                _brand = ctx.Brands.SingleOrDefault(x => x.brandId == id);
            }

            return _brand;
        }

        public int CreateBrand(Brand brand)
        {
            using (RentCarContext ctx = new RentCarContext())
            {
                

                try
                {
                    ctx.Brands.Add(brand);
                    return ctx.SaveChanges();
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }

        }

        public int UpdateBrand(Brand brand)
        {
            Brand _brand;
            using (RentCarContext ctx = new RentCarContext())
            {
                _brand = ctx.Brands.SingleOrDefault(x => x.brandId == brand.brandId);

                if (_brand == null)
                {
                    return 0;
                }

                _brand.description = brand.description;
                _brand.state = brand.state;

                return ctx.SaveChanges();
            }
        }

        public int DeleteBrand(int id)
        {
            using (RentCarContext ctx = new RentCarContext())
            {
                try
                {
                    var b = new Brand { brandId = id };
                    ctx.Entry(b).State = EntityState.Deleted;

                    return ctx.SaveChanges();
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        public List<Brand> getBrandsFilter(String seach)
        {
            using (var ctx = new RentCarContext())
            {
                List<Brand> brands;

                brands = ctx.Brands.Where(x => x.description.Contains(seach)).Where(b => b.state == "Activo")
                  .ToList();
                
           


                return brands;
            }
        }


    }
}
