﻿using Datos;
using Datos.Models;
using Negocio.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentCar.Controllers
{
    public class UserController
    {

        public IEnumerable<User> getUsers()
        {
            using (var ctx = new RentCarContext())
            {
                return ctx.Users;
            }
        }

        public User GetUserbyEmail(string email)
        {
            User _user;

            using (RentCarContext ctx = new RentCarContext())
            {
                _user = ctx.Users.SingleOrDefault(x => x.email == email);
                return _user;
            }

        }

        public User GetUserbyId(int id)
        {
            User _user;

            using (RentCarContext ctx = new RentCarContext())
            {
                _user = ctx.Users.SingleOrDefault(x => x.userId == id);
                return _user;
            }

        }


        public int CreateUser(User user)
        {
            int id = 0;
            using (var ctx = new RentCarContext())
            {
                var role = ctx.Roles.Find(2);
                
                user.role = role;
                string password = user.password;
               // user.setPassword(password);
                ctx.Users.Add(user);

               var response =  ctx.SaveChanges();
                if (response > 0)
                {
                    MessageBox.Show("El Usuario se creo correctamente", "Se creo correctamente", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    id = user.userId;
                }
                else
                {
                    MessageBox.Show("El Usuario no se pudo guardar",
                                    "Error al guardar los datos",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);

                }

            }
            return id;
        }

        public Employee ValidateCredentials(User user)
        {
            using (var ctx = new RentCarContext())
            {
                User _user = null;
                Employee employee = null;
                _user = ctx.Users.SingleOrDefault(u => u.email == user.email);
                if (_user != null)
                {
                    if (_user.password != user.password)
                    {

                        return employee;
                    }
                    EmployeeController emplo = new EmployeeController();
                    employee =  emplo.getEmployeeByUser(_user.userId);

                }
                return employee;

                

            }
        }
    }
}
