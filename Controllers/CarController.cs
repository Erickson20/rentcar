﻿using Datos;
using Datos.Models;
using RentCar.Views;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentCar.Controllers
{
    public class CarController
    {

        public List<Car> getCars()
        {
            using (var ctx = new RentCarContext())
            {
                var _cars = ctx.Cars
                   .Include("Model")
                   .Include("Model.brand")
                   .Include("FuelType")
                   .Include("CarType")
                   .ToList();

                return _cars;
            }
        }

        public List<Car> getCarsActived()
        {
            using (var ctx = new RentCarContext())
            {
                var _cars = ctx.Cars
                   .Include("Model")
                   .Include("Model.brand")
                   .Include("FuelType")
                   .Include("CarType").Where(b => b.state == "Activo")
                   .ToList();

                return _cars;
            }
        }
        public List<Car> getCarsFilter(String filter, String seach)
        {
            using (var ctx = new RentCarContext())
            {
                List<Car> _cars;
                if (filter == "descripcion")
                {
                     _cars = ctx.Cars
                   .Include("Model")
                   .Include("Model.brand")
                   .Include("FuelType")
                   .Include("CarType").Where(x => x.description.Contains(seach)).Where(b => b.state == "Activo")
                   .ToList();
                }
                else
                {
                     _cars = ctx.Cars
                   .Include("Model")
                   .Include("Model.brand")
                   .Include("FuelType")
                   .Include("CarType").Where(x => x.chasisNumber.ToString().Contains(seach)).Where(b => b.state == "Activo")
                   .ToList();
                }
                

                return _cars;
            }
        }



        public DataTable ConvertToDataTable(List<Car> cars)
        {
            DataTable dt = new DataTable();
            DataColumn dc = new DataColumn("ID", typeof(String));
            dt.Columns.Add(dc);

            dc = new DataColumn("Vehiculo", typeof(String));
            dt.Columns.Add(dc);

            dc = new DataColumn("Chasis No.", typeof(String));
            dt.Columns.Add(dc);

            dc = new DataColumn("Combustible", typeof(String));
            dt.Columns.Add(dc);

            dc = new DataColumn("Marca", typeof(String));
            dt.Columns.Add(dc);

            dc = new DataColumn("Modelo", typeof(String));
            dt.Columns.Add(dc);

            dc = new DataColumn("Tipo", typeof(String));
            dt.Columns.Add(dc);

            dc = new DataColumn("Motor No.", typeof(String));
            dt.Columns.Add(dc);

            dc = new DataColumn("Placa No.", typeof(String));
            dt.Columns.Add(dc);

            dc = new DataColumn("Estado", typeof(String));
            dt.Columns.Add(dc);

            foreach (Car car in cars)
            {
                var s = car;
                DataRow row = dt.NewRow();
                row[0] = car.carId;
                row[1] = car.description;
                row[2] = car.chasisNumber;
                row[3] = car.fuelType != null ? car.fuelType.description : String.Empty;
                row[4] = car.model != null ? car.model.brand.description : String.Empty;
                row[5] = car.model != null ? car.model.description : String.Empty;
                row[6] = car.carType != null ? car.carType.description : String.Empty;
                row[7] = car.motorNumber;
                row[8] = car.plateNumber;
                row[9] = car.state;

                dt.Rows.Add(row);
            }

            return dt;

        }
        public Car getCarById(int id)
        {
            using (var ctx = new RentCarContext())
            {
                var result = ctx.Cars.SingleOrDefault(b => b.carId == id);
                return result;
            }
        }

        public int CreateCar(Car car)
        {
            using (var ctx = new RentCarContext())
            {
               
                ctx.Cars.Add(car);

                
                try
                {
                   
                    return ctx.SaveChanges();
                }
                catch (Exception ex) {
                    return 0;
                }
            }
                
            
        }

        public int UpdateCar(Car car, int id)
        {
            using (var ctx = new RentCarContext())
            {
                var result = ctx.Cars.SingleOrDefault(b => b.carId == id);
                ctx.Cars.Attach(result);
                    try
                    {

                    result.chasisNumber = car.chasisNumber;
                    result.motorNumber = car.motorNumber;
                    result.plateNumber = car.plateNumber;
                    result.carTypeId = car.carTypeId;
                    result.fuelType = car.fuelType;
                    result.modelId = result.modelId;
                    result.state = car.state;
                    result.description = car.description;
                    
                    

                    return ctx.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        return 0;
                    }
                
            }


        }



        public int Update(Car car)
        {
            using (RentCarContext ctx = new RentCarContext() )
            {
                var _car = ctx.Cars.Find(car.carId);
                if (_car == null)
                {
                    return 0;
                }

                ctx.Entry(_car).CurrentValues.SetValues(car);
                return ctx.SaveChanges();

            }
           
        }

        public int DeleteCar(int id)
        {
            using (var ctx = new RentCarContext())
            {
                try
                {
                    
                    var car = new Car { carId = id };
                    ctx.Cars.Attach(car);
                    ctx.Cars.Remove(car);

                    return ctx.SaveChanges();
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }


        }
    }
}
