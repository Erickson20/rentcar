﻿using Datos;
using Datos.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentCar.Controllers
{
    public class RentController
    {
        public int Rent(Client client )
        {
            return 0;
        }

        public List<Rent> getRents()
        {
            using (var ctx = new RentCarContext())
            {
                return   ctx.Rents.Where(b => b.returned == false).ToList();

                
            }
        }
        public Rent getRentById(int id)
        {
            using (var ctx = new RentCarContext())
            {
                var result = ctx.Rents.SingleOrDefault(b => b.rentId == id);
                return result;
            }
        }

        public List<Rent> getRentsFilter(int seach)
        {
            using (var ctx = new RentCarContext())
            {
                List<Rent> c;
                
                    c = ctx.Rents.Where(b => b.rentId == seach)
                  .ToList();
                
               


                return c;
            }
        }

        public int CreateRent(Rent r)
        {
            using (var ctx = new RentCarContext())
            {

                ctx.Rents.Add(r);


                try
                {
                    
                    return ctx.SaveChanges();
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }


        }

        public int UpdateRentRetun(Rent r, int id)
        {
            using (var ctx = new RentCarContext())
            {
                var result = ctx.Rents.SingleOrDefault(b => b.rentId == id);
               
                try
                {  
                    result.returned = true;
                    result.total = r.total;

                    return ctx.SaveChanges();
                }
                catch (Exception ex)
                {
                    return 0;
                }

            }


        }

        public int DeleteRent(int id)
        {
            using (RentCarContext ctx = new RentCarContext())
            {
                try
                {
                    Rent r = new Rent { carId= id };

                    ctx.Entry(r).State = EntityState.Deleted;

                    return ctx.SaveChanges();
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }
    }


}
