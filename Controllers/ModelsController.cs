﻿using Datos;
using Datos.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentCar.Controllers
{
    public class ModelsController
    {
        public List<Model> getModels()
        {
            List<Model> _models;
            using (RentCarContext ctx = new RentCarContext())
            {

                _models = ctx.Models.ToList();
            }

            return _models;
        }

        public Model getModelById(int id)
        {
            using (var ctx = new RentCarContext())
            {
                var result = ctx.Models.SingleOrDefault(b => b.id == id);
                return result;
            }
        }

        public int CreateModel(Model model)
        {
            using (RentCarContext ctx = new RentCarContext())
            {
                
                try
                {
                    ctx.Models.Add(model);
                    return ctx.SaveChanges();
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }

        }

        public int DeleteModel(int id)
        {
            using (RentCarContext ctx = new RentCarContext())
            {
                try
                {
                    var m = new Model { id = id };
                    ctx.Entry(m).State = EntityState.Deleted;

                    return ctx.SaveChanges();
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        public List<Model> getModelByBrand(int id)
        {

            List<Model> _models;
            using (RentCarContext ctx = new RentCarContext())
            {
               
                _models = ctx.Models.Where(x=> x.brandId==id).ToList();
            }

            return _models;

        }

        public List<Model> getModelFilter(String seach)
        {
            using (var ctx = new RentCarContext())
            {
                List<Model> m;

                m = ctx.Models.Where(x => x.description.Contains(seach)).Where(b => b.state == "Activo")
                  .ToList();

                return m;
            }
        }
    }
}
