﻿using Datos;
using Datos.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Controllers
{
    public class EmployeeController
    {
            public List<Employee> getEmployees()
            {
                using (var ctx = new RentCarContext())
                {
                    return ctx.Employees.ToList();
                }
            }



            public Employee getEmployeeById(int id)
            {
                using (var ctx = new RentCarContext())
                {
                    var result = ctx.Employees.SingleOrDefault(b => b.employeeId == id);
                    return result;
                }
            }

            public Employee getEmployeeByUser(int id)
            {
                using (var ctx = new RentCarContext())
                {
                    var result = ctx.Employees.SingleOrDefault(b => b.userId == id);
                    return result;
                }
            }

        public int CreateEmployee(Employee emplo)
                {
                    using (var ctx = new RentCarContext())
                    {

                        ctx.Employees.Add(emplo);


                        try
                        {

                            return ctx.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            return 0;
                        }
                    }
             }

            public int DeleteEmployee(int id)
            {
                using (var ctx = new RentCarContext())
                {


                    try
                    {
                        var emplo = new Employee { employeeId = id };
                        ctx.Entry(emplo).State = EntityState.Deleted;

                        return ctx.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        return 0;
                    }
                }


            }

            public int UpdateEmployee(Employee emplo, int id)
            {
                using (var ctx = new RentCarContext())
                {



                    var result = ctx.Employees.SingleOrDefault(b => b.employeeId == id);


                    try
                    {

                        result.name = emplo.name;
                        result.firstLastName = emplo.firstLastName;
                        result.secondLastName = emplo.secondLastName;
                        result.citizenId = emplo.citizenId;
                        result.commissionPorcent = emplo.commissionPorcent;
                        result.workshift = emplo.workshift;
                        result.state = emplo.state;
                        result.hireDate = emplo.hireDate;
                        



                        return ctx.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        return 0;
                    }

                }


            }

        public List<Employee> getEmployeeFilter(String filter, String seach)
        {
            using (var ctx = new RentCarContext())
            {
                List<Employee> c;
                if (filter == "Nombre")
                {
                    c = ctx.Employees.Where(x => x.name.Contains(seach)).Where(b => b.state == "Activo")
                  .ToList();
                }
                else
                {
                    c = ctx.Employees.Where(x => x.citizenId.Contains(seach)).Where(b => b.state == "Activo")
                  .ToList();
                }


                return c;
            }
        }


    }
 }
