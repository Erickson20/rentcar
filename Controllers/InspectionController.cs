﻿using Datos;
using Datos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentCar.Controllers
{
    public class InspectionController
    {

        public int CreateInspection(Inspection i)
        {
            using (RentCarContext ctx = new RentCarContext())
            {

                ctx.Inspections.Add(i);
                return ctx.SaveChanges();
            }

        }

        public Inspection InspectionsByRent(int i)
        {
            using (RentCarContext ctx = new RentCarContext())
            {

                
                var result = ctx.Inspections.SingleOrDefault(b => b.rentId == i);
                return result;
            }

        }



        public int UpdateInspection(Inspection i, int id)
        {
            using (var ctx = new RentCarContext())
            {



                var result = ctx.Inspections.SingleOrDefault(b => b.rentId == id);


                try
                {

                    // result.rentId = i.rentId;
                    result.car.carId = i.car.carId;
                    result.amountOfFuel = i.amountOfFuel;
                    result.employeeId = i.employeeId;
                    result.date = i.date;
                    result.hasCrystalCrashes = i.hasCrystalCrashes;
                    result.hasHydraulicJack = i.hasHydraulicJack;
                    result.hasResponseTire = i.hasResponseTire;
                    result.hasScratches = i.hasScratches;
                    result.InspectionType = i.InspectionType;
                    result.status = i.status;
                    result.tyreBacktLeft = i.tyreBacktLeft;
                    result.tyreBacktRigth = i.tyreBacktRigth;
                    result.tyreFrontLeft = i.tyreFrontLeft;
                    result.tyreFrontRigth = i.tyreFrontRigth;

                    return ctx.SaveChanges();
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }
    }
}
