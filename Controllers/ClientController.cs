﻿using Datos;
using Datos.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Controllers
{
    public class ClientController
    {

        public List<Client> GetClients()
        {

            using (var ctx = new RentCarContext())
            {
                return ctx.Clients.ToList();
            }

        }

        public Client GetClient(int id)
        {
            Client _client;

            using (RentCarContext ctx = new RentCarContext())
            {
                _client = ctx.Clients.SingleOrDefault(x => x.clientId == id);
                return _client;
            }

        }

        public int CreateClient(Client client)
        {
            using (RentCarContext ctx = new RentCarContext())
            {
                ctx.Clients.Add(client);
                return ctx.SaveChanges();
            }

        }

        public int UpdateClient(Client client, int id)
        {
            using (var ctx = new RentCarContext())
            {



                var result = ctx.Clients.SingleOrDefault(b => b.clientId == id);


                try
                {

                    result.name = client.name;
                    result.firstLastName = client.firstLastName;
                    result.secondLastName = client.secondLastName;
                    result.citizenId = client.citizenId;
                    result.creditCarNumber = client.creditCarNumber;
                    result.creditLimit = client.creditLimit;
                    result.state = client.state;
                    result.personType = client.personType;




                    return ctx.SaveChanges();
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        public int DeleteClient(int id)
        {
            using (RentCarContext ctx = new RentCarContext())
            {
                try
                {
                    var client = new Client { clientId = id };
                    ctx.Entry(client).State = EntityState.Deleted;

                    return ctx.SaveChanges();
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        public List<Client> getClientFilter(String filter, String seach)
        {
            using (var ctx = new RentCarContext())
            {
                List<Client> c;
                if (filter == "Nombre")
                {
                    c = ctx.Clients.Where(x => x.name.Contains(seach)).Where(b => b.state == "Activo")
                  .ToList();
                }
                else
                {
                    c = ctx.Clients.Where(x => x.citizenId.Contains(seach)).Where(b => b.state == "Activo")
                  .ToList();
                }


                return c;
            }
        }
    }
}
