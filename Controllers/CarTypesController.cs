﻿using Datos;
using Datos.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentCar.Controllers
{
    public class CarTypesController
    {

        public List<CarType> getCarTypes()
        {
            List<CarType> _carType;
            using (RentCarContext ctx = new RentCarContext())
            {

                _carType = ctx.CarTypes.ToList();
                
            }

            return _carType;
        }

        public CarType GetCarTypeById(int id)
        {
            CarType c;

            using (RentCarContext ctx = new RentCarContext())
            {
                c = ctx.CarTypes.SingleOrDefault(x => x.carTypeId == id);
                return c;
            }

        }

        public int CreateCarTypes(CarType carType)
        {
            using (RentCarContext ctx = new RentCarContext())
            {
                
                try
                {
                    ctx.CarTypes.Add(carType);
                    return ctx.SaveChanges();
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }

        }

        public int DeleteCarTypes(int id)
        {
            using (RentCarContext ctx = new RentCarContext())
            {
                try
                {
                    var c = new CarType { carTypeId = id };
                    ctx.Entry(c).State = EntityState.Deleted;

                    return ctx.SaveChanges();
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }

        public List<CarType> getTypesFilter(String seach)
        {
            using (var ctx = new RentCarContext())
            {
                List<CarType> c;

                c = ctx.CarTypes.Where(x => x.description.Contains(seach)).Where(b => b.state == "Activo")
                  .ToList();

                return c;
            }
        }
    }
}
