﻿using Datos;
using Datos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.Controllers
{
    public class UserController
    {

        public IEnumerable<User> getUsers()
        {
            using (var ctx = new RentCarContext())
            {
                return ctx.Users;
            }
        }

        public void CreateUser(User user)
        {
            using (var ctx = new RentCarContext())
            {
                string password = user.password;
                user.setPassword(password);
                ctx.Users.Add(user);
            }
        }

        public User ValidateCredentials(string username, string password)
        {
            using (var ctx = new RentCarContext())
            {
                User user = null;
                user = ctx.Users.SingleOrDefault(u=> u.userName == username);
                if (user != null)
                {
                    if (password != user.getPassword()) {
                        user = null;
                    }
                        
                }

                return user;

            }
        }
    }
}
