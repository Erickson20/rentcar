﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using RentCar.Reports;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RentCar.Views
{
    public partial class ReportView : Form
    {
        public ReportView()
        {
            InitializeComponent();
        }

        private void ReportView_Load(object sender, EventArgs e)
        {

        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            SalesReport sales = new SalesReport();
            ParameterFields paramters = new ParameterFields();

            ParameterField dateRentFrom = new ParameterField();
            dateRentFrom.Name = "@dateRentFrom";

            ParameterField dateRentTo = new ParameterField();
            dateRentTo.Name = "@dateRentTo";

            ParameterDiscreteValue toValue = new ParameterDiscreteValue();
            toValue.Value = dateTimePicker1.Value.ToShortDateString();

            ParameterDiscreteValue fromValue = new ParameterDiscreteValue();
            fromValue.Value = dateTimePicker2.Value.ToShortDateString();


            dateRentFrom.CurrentValues.Add(fromValue);
            paramters.Add(dateRentFrom);

            dateRentTo.CurrentValues.Add(toValue);
            paramters.Add(dateRentTo);
            crystalReportViewer1.ParameterFieldInfo = paramters;
            


            // var s = sales.DataDefinition.ParameterFields;
            
           // sales.SetParameterValue(1, dateTimePicker2.Value.ToString("yyyy-MM-dd"));
            crystalReportViewer1.ReportSource = sales;
        }
    }
}
