﻿using Datos.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
   public class RentCarContext: DbContext
    {
        public RentCarContext(): base("name=RentCarDBConnectionString")
        {
           
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            Database.SetInitializer<RentCarContext>(null);
            base.OnModelCreating(modelBuilder);
            
       
        }



        public DbSet<Brand> Brands { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<CarType> CarTypes { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<FuelType> FuelTypes { get; set; }
        public DbSet<Inspection> Inspections { get; set; }
        public DbSet<Model> Models { get; set; }
        public DbSet<Rent> Rents { get; set; }
        public DbSet<Permission> Permisions { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
    }


}
