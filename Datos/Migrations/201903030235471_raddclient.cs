namespace Datos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class raddclient : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Rents", "client_clientId", "dbo.Clients");
            DropForeignKey("dbo.Rents", "car_carId", "dbo.Cars");
            DropForeignKey("dbo.Rents", "employee_employeeId", "dbo.Employees");
            DropIndex("dbo.Rents", new[] { "car_carId" });
            DropIndex("dbo.Rents", new[] { "client_clientId" });
            DropIndex("dbo.Rents", new[] { "employee_employeeId" });
            RenameColumn(table: "dbo.Rents", name: "client_clientId", newName: "clientId");
            RenameColumn(table: "dbo.Rents", name: "car_carId", newName: "carId");
            RenameColumn(table: "dbo.Rents", name: "employee_employeeId", newName: "employeeId");
            AlterColumn("dbo.Rents", "carId", c => c.Int(nullable: false));
            AlterColumn("dbo.Rents", "clientId", c => c.Int(nullable: false));
            AlterColumn("dbo.Rents", "employeeId", c => c.Int(nullable: false));
            CreateIndex("dbo.Rents", "carId");
            CreateIndex("dbo.Rents", "clientId");
            CreateIndex("dbo.Rents", "employeeId");
            AddForeignKey("dbo.Rents", "clientId", "dbo.Clients", "clientId", cascadeDelete: true);
            AddForeignKey("dbo.Rents", "carId", "dbo.Cars", "carId", cascadeDelete: true);
            AddForeignKey("dbo.Rents", "employeeId", "dbo.Employees", "employeeId", cascadeDelete: true);

        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Rents", "employeeId", "dbo.Employees");
            DropForeignKey("dbo.Rents", "carId", "dbo.Cars");
            DropForeignKey("dbo.Rents", "clientId", "dbo.Clients");
            DropIndex("dbo.Rents", new[] { "employeeId" });
            DropIndex("dbo.Rents", new[] { "clientId" });
            DropIndex("dbo.Rents", new[] { "carId" });
            AlterColumn("dbo.Rents", "employeeId", c => c.Int());
            AlterColumn("dbo.Rents", "clientId", c => c.Int());
            AlterColumn("dbo.Rents", "carId", c => c.Int());
            RenameColumn(table: "dbo.Rents", name: "employeeId", newName: "employee_employeeId");
            RenameColumn(table: "dbo.Rents", name: "carId", newName: "car_carId");
            RenameColumn(table: "dbo.Rents", name: "clientId", newName: "client_clientId");
            CreateIndex("dbo.Rents", "employee_employeeId");
            CreateIndex("dbo.Rents", "client_clientId");
            CreateIndex("dbo.Rents", "car_carId");
            AddForeignKey("dbo.Rents", "employee_employeeId", "dbo.Employees", "employeeId");
            AddForeignKey("dbo.Rents", "car_carId", "dbo.Cars", "carId");
            AddForeignKey("dbo.Rents", "client_clientId", "dbo.Clients", "clientId");
        }
    }
}
