namespace Datos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class testing3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Inspections", "rentId", c => c.Int(nullable: false));
            CreateIndex("dbo.Inspections", "rentId");
            AddForeignKey("dbo.Inspections", "rentId", "dbo.Rents", "rentId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Inspections", "rentId", "dbo.Rents");
            DropIndex("dbo.Inspections", new[] { "rentId" });
            DropColumn("dbo.Inspections", "rentId");
        }
    }
}
