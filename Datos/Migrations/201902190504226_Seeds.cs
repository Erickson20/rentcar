namespace Datos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Seeds : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Cars", "brandId", "dbo.Brands");
            DropIndex("dbo.Cars", new[] { "brandId" });
            AddColumn("dbo.Models", "brand_brandId", c => c.Int());
            CreateIndex("dbo.Models", "brand_brandId");
            AddForeignKey("dbo.Models", "brand_brandId", "dbo.Brands", "brandId");
            DropColumn("dbo.Cars", "brandId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Cars", "brandId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Models", "brand_brandId", "dbo.Brands");
            DropIndex("dbo.Models", new[] { "brand_brandId" });
            DropColumn("dbo.Models", "brand_brandId");
            CreateIndex("dbo.Cars", "brandId");
            AddForeignKey("dbo.Cars", "brandId", "dbo.Brands", "brandId", cascadeDelete: true);
        }
    }
}
