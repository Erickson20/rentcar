namespace Datos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class atributtebrandId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Models", "brand_brandId", "dbo.Brands");
            DropIndex("dbo.Models", new[] { "brand_brandId" });
            RenameColumn(table: "dbo.Models", name: "brand_brandId", newName: "brandId");
            AlterColumn("dbo.Models", "brandId", c => c.Int(nullable: false));
            CreateIndex("dbo.Models", "brandId");
            AddForeignKey("dbo.Models", "brandId", "dbo.Brands", "brandId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Models", "brandId", "dbo.Brands");
            DropIndex("dbo.Models", new[] { "brandId" });
            AlterColumn("dbo.Models", "brandId", c => c.Int());
            RenameColumn(table: "dbo.Models", name: "brandId", newName: "brand_brandId");
            CreateIndex("dbo.Models", "brand_brandId");
            AddForeignKey("dbo.Models", "brand_brandId", "dbo.Brands", "brandId");
        }
    }
}
