namespace Datos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Users : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Permissions",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        description = c.String(),

                        Role_id = c.Int(),

                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Roles", t => t.Role_id)
                .Index(t => t.Role_id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        userId = c.Int(nullable: false, identity: true),
                        firstName = c.String(),
                        lastName = c.String(),
                        email = c.String(),
                        role_id = c.Int(),
                    })
                .PrimaryKey(t => t.userId)
                .ForeignKey("dbo.Roles", t => t.role_id)
                .Index(t => t.role_id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        rolName = c.String(),
                        description = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Users", "role_id", "dbo.Roles");
            DropForeignKey("dbo.Permissions", "Role_id", "dbo.Roles");
            DropIndex("dbo.Users", new[] { "role_id" });
            DropIndex("dbo.Permissions", new[] { "Role_id" });
            DropTable("dbo.Roles");
            DropTable("dbo.Users");
            DropTable("dbo.Permissions");
        }
    }
}
