namespace Datos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RentInspectionWithId : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Inspections", "car_carId", "dbo.Cars");
            DropIndex("dbo.Inspections", new[] { "car_carId" });
            RenameColumn(table: "dbo.Inspections", name: "car_carId", newName: "carId");
            AlterColumn("dbo.Inspections", "carId", c => c.Int(nullable: false));
            CreateIndex("dbo.Inspections", "carId");
            AddForeignKey("dbo.Inspections", "carId", "dbo.Cars", "carId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Inspections", "carId", "dbo.Cars");
            DropIndex("dbo.Inspections", new[] { "carId" });
            AlterColumn("dbo.Inspections", "carId", c => c.Int());
            RenameColumn(table: "dbo.Inspections", name: "carId", newName: "car_carId");
            CreateIndex("dbo.Inspections", "car_carId");
            AddForeignKey("dbo.Inspections", "car_carId", "dbo.Cars", "carId");
        }
    }
}
