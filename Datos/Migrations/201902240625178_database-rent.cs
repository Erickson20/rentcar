namespace Datos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class databaserent : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Returns", "rentReference_id", "dbo.Rents");
            DropForeignKey("dbo.Returns", "Client_clientId", "dbo.Clients");
            DropIndex("dbo.Returns", new[] { "rentReference_id" });
            DropIndex("dbo.Returns", new[] { "Client_clientId" });
            DropTable("dbo.Returns");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Returns",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        rentReference_id = c.Int(),
                        Client_clientId = c.Int(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateIndex("dbo.Returns", "Client_clientId");
            CreateIndex("dbo.Returns", "rentReference_id");
            AddForeignKey("dbo.Returns", "Client_clientId", "dbo.Clients", "clientId");
            AddForeignKey("dbo.Returns", "rentReference_id", "dbo.Rents", "id");
        }
    }
}
