namespace Datos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StoreProceduresForReports : DbMigration
    {
        public override void Up()
        {
            CreateStoredProcedure("sp_sales_by_date", x => new { dateRentFrom = x.DateTime(), dateRentTo = x.DateTime() },
                "SELECT * FROM Rents where dateRent  between @dateRentFrom and @dateRentTo");
            {

            }
        }
        
        public override void Down()
        {
            DropStoredProcedure("sp_sales_by_date");
        }
    }
}
