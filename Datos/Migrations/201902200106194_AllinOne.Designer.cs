// <auto-generated />
namespace Datos.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AllinOne : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AllinOne));
        
        string IMigrationMetadata.Id
        {
            get { return "201902200106194_AllinOne"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
