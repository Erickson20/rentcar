namespace Datos.Migrations
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Datos.RentCarContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Datos.RentCarContext context)
        {
             //Roles
            context.Roles.AddOrUpdate(x => x.roleId,
             new Role() { rolName = "Administrador", description = "Admin" },
             new Role() { rolName = "Empleado", description = "Employee" }

             );

            context.FuelTypes.AddOrUpdate(x => x.fuelTypeId,
              new FuelType { description = "Gasolina", state = "Activo" },
              new FuelType { description = "Gasoil", state = "Activo" },
              new FuelType { description = "Gas", state = "Activo" });

            context.CarTypes.AddOrUpdate(x => x.description,
               new CarType { description = "Sed�n", state = "Activo" });


            context.SaveChanges();

        }
    }
}
