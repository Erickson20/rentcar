namespace Datos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dbrentcart2 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.RolePermissions", newName: "PermissionRoles");
            DropPrimaryKey("dbo.PermissionRoles");
            AddColumn("dbo.Rents", "dateRent", c => c.DateTime(nullable: false));
            AddColumn("dbo.Rents", "costByDay", c => c.Int(nullable: false));
            AddColumn("dbo.Rents", "rentNumber", c => c.Int(nullable: false));
            AddColumn("dbo.Rents", "days", c => c.Int(nullable: false));
            AddColumn("dbo.Rents", "dateReturn", c => c.DateTime(nullable: false));
            AddColumn("dbo.Employees", "userId", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.PermissionRoles", new[] { "Permission_id", "Role_roleId" });
            CreateIndex("dbo.Employees", "userId");
            AddForeignKey("dbo.Employees", "userId", "dbo.Users", "userId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Employees", "userId", "dbo.Users");
            DropIndex("dbo.Employees", new[] { "userId" });
            DropPrimaryKey("dbo.PermissionRoles");
            DropColumn("dbo.Employees", "userId");
            DropColumn("dbo.Rents", "dateReturn");
            DropColumn("dbo.Rents", "days");
            DropColumn("dbo.Rents", "rentNumber");
            DropColumn("dbo.Rents", "costByDay");
            DropColumn("dbo.Rents", "dateRent");
            AddPrimaryKey("dbo.PermissionRoles", new[] { "Role_roleId", "Permission_id" });
            RenameTable(name: "dbo.PermissionRoles", newName: "RolePermissions");
        }
    }
}
