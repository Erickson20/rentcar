namespace Datos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RoleIdAdded : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Users", "role_id", "dbo.Roles");
            DropForeignKey("dbo.RolePermissions", "Role_id", "dbo.Roles");
            DropIndex("dbo.Users", new[] { "role_id" });
            RenameColumn(table: "dbo.RolePermissions", name: "Role_id", newName: "Role_roleId");
            RenameColumn(table: "dbo.Users", name: "role_id", newName: "roleId");
            RenameIndex(table: "dbo.RolePermissions", name: "IX_Role_id", newName: "IX_Role_roleId");
            
            DropPrimaryKey("dbo.Roles");
            DropColumn("dbo.Roles", "id");
            AddColumn("dbo.Roles", "roleId", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Users", "roleId", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Roles", "roleId");
            CreateIndex("dbo.Users", "roleId");
            AddForeignKey("dbo.Users", "roleId", "dbo.Roles", "roleId", cascadeDelete: true);
            AddForeignKey("dbo.RolePermissions", "Role_roleId", "dbo.Roles", "roleId", cascadeDelete: true);
           
        }
        
        public override void Down()
        {
            AddColumn("dbo.Roles", "id", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.RolePermissions", "Role_roleId", "dbo.Roles");
            DropForeignKey("dbo.Users", "roleId", "dbo.Roles");
            DropIndex("dbo.Users", new[] { "roleId" });
            DropPrimaryKey("dbo.Roles");
            AlterColumn("dbo.Users", "roleId", c => c.Int());
            DropColumn("dbo.Roles", "roleId");
            AddPrimaryKey("dbo.Roles", "id");
            RenameIndex(table: "dbo.RolePermissions", name: "IX_Role_roleId", newName: "IX_Role_id");
            RenameColumn(table: "dbo.Users", name: "roleId", newName: "role_id");
            RenameColumn(table: "dbo.RolePermissions", name: "Role_roleId", newName: "Role_id");
            CreateIndex("dbo.Users", "role_id");
            AddForeignKey("dbo.RolePermissions", "Role_id", "dbo.Roles", "id", cascadeDelete: true);
            AddForeignKey("dbo.Users", "role_id", "dbo.Roles", "id");
        }
    }
}
