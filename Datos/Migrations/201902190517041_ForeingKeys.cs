namespace Datos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ForeingKeys : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Returns", "Client_clientId", c => c.Int());
            CreateIndex("dbo.Returns", "Client_clientId");
            AddForeignKey("dbo.Returns", "Client_clientId", "dbo.Clients", "clientId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Returns", "Client_clientId", "dbo.Clients");
            DropIndex("dbo.Returns", new[] { "Client_clientId" });
            DropColumn("dbo.Returns", "Client_clientId");
        }
    }
}
