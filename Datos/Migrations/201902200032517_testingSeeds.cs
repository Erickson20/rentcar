namespace Datos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class testingSeeds : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Permissions", "Role_id", "dbo.Roles");
            DropIndex("dbo.Permissions", new[] { "Role_id" });
            CreateTable(
                "dbo.RolePermissions",
                c => new
                    {
                        Role_id = c.Int(nullable: false),
                        Permission_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Role_id, t.Permission_id })
                .ForeignKey("dbo.Roles", t => t.Role_id, cascadeDelete: true)
                .ForeignKey("dbo.Permissions", t => t.Permission_id, cascadeDelete: true)
                .Index(t => t.Role_id)
                .Index(t => t.Permission_id);
            
            DropColumn("dbo.Permissions", "Role_id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Permissions", "Role_id", c => c.Int());
            DropForeignKey("dbo.RolePermissions", "Permission_id", "dbo.Permissions");
            DropForeignKey("dbo.RolePermissions", "Role_id", "dbo.Roles");
            DropIndex("dbo.RolePermissions", new[] { "Permission_id" });
            DropIndex("dbo.RolePermissions", new[] { "Role_id" });
            DropTable("dbo.RolePermissions");
            CreateIndex("dbo.Permissions", "Role_id");
            AddForeignKey("dbo.Permissions", "Role_id", "dbo.Roles", "id");
        }
    }
}
