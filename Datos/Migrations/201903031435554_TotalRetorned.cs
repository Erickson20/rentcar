namespace Datos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TotalRetorned : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Rents", "total", c => c.Single());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Rents", "total");
        }
    }
}
