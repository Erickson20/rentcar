namespace Datos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class testing2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Inspections", "inspectionId", "dbo.Rents");
            DropIndex("dbo.Inspections", new[] { "inspectionId" });
            DropPrimaryKey("dbo.Rents");
            DropPrimaryKey("dbo.Inspections");
            AlterColumn("dbo.Rents", "rentId", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Rents", "rentNumber", c => c.Int(nullable: false));
            AlterColumn("dbo.Inspections", "InspectionId", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Rents", "rentId");
            AddPrimaryKey("dbo.Inspections", "InspectionId");
            DropColumn("dbo.Inspections", "rentId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Inspections", "rentId", c => c.Int(nullable: false));
            DropPrimaryKey("dbo.Inspections");
            DropPrimaryKey("dbo.Rents");
            AlterColumn("dbo.Inspections", "InspectionId", c => c.Int(nullable: false));
            AlterColumn("dbo.Rents", "rentNumber", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Rents", "rentId", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Inspections", "inspectionId");
            AddPrimaryKey("dbo.Rents", "rentId");
            CreateIndex("dbo.Inspections", "inspectionId");
            AddForeignKey("dbo.Inspections", "inspectionId", "dbo.Rents", "rentId");
        }
    }
}
