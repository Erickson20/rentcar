namespace Datos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RentInspection : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Rents");
            DropPrimaryKey("dbo.Inspections");
            AddColumn("dbo.Rents", "rentId", c => c.Int(nullable: false,identity:true));
            AddColumn("dbo.Inspections", "tyreFrontLeft", c => c.Boolean(nullable: false));
            AddColumn("dbo.Inspections", "tyreFrontRigth", c => c.Boolean(nullable: false));
            AddColumn("dbo.Inspections", "tyreBacktLeft", c => c.Boolean(nullable: false));
            AddColumn("dbo.Inspections", "tyreBacktRigth", c => c.Boolean(nullable: false));
            AddColumn("dbo.Inspections", "tyreExtra", c => c.Boolean(nullable: false));
            AddColumn("dbo.Inspections", "rentId", c => c.Int(nullable: false));
            AddColumn("dbo.Inspections", "InspectionType", c => c.String());
            AlterColumn("dbo.Rents", "rentNumber", c => c.Int(nullable: false));
            AlterColumn("dbo.Inspections", "inspectionId", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Rents", "rentId");
            AddPrimaryKey("dbo.Inspections", "inspectionId");
            CreateIndex("dbo.Inspections", "inspectionId");
            AddForeignKey("dbo.Inspections", "inspectionId", "dbo.Rents", "rentId");
            DropColumn("dbo.Rents", "id");
          
        }
        
        public override void Down()
        {
           
            DropForeignKey("dbo.Inspections", "inspectionId", "dbo.Rents");
            DropIndex("dbo.Inspections", new[] { "inspectionId" });
            DropPrimaryKey("dbo.Inspections");
            DropPrimaryKey("dbo.Rents");
          
            AlterColumn("dbo.Rents", "rentNumber", c => c.Int(nullable: false));
            DropColumn("dbo.Inspections", "InspectionType");
            DropColumn("dbo.Inspections", "rentId");
            DropColumn("dbo.Inspections", "tyreExtra");
            DropColumn("dbo.Inspections", "tyreBacktRigth");
            DropColumn("dbo.Inspections", "tyreBacktLeft");
            DropColumn("dbo.Inspections", "tyreFrontRigth");
            DropColumn("dbo.Inspections", "tyreFrontLeft");
            DropColumn("dbo.Rents", "rentId");
            AddPrimaryKey("dbo.Inspections", "inspectionId");
            AddPrimaryKey("dbo.Rents", "id");
        }
    }
}
