namespace Datos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Brands",
                c => new
                    {
                        brandId = c.Int(nullable: false, identity: true),
                        description = c.String(),
                        state = c.String(),
                    })
                .PrimaryKey(t => t.brandId);
            
            CreateTable(
                "dbo.Cars",
                c => new
                    {
                        carId = c.Int(nullable: false, identity: true),
                        description = c.String(),
                        chasisNumber = c.Int(nullable: false),
                        motorNumber = c.Int(nullable: false),
                        plateNumber = c.Int(nullable: false),
                        carTypeId = c.Int(nullable: false),
                        brandId = c.Int(nullable: false),
                        modelId = c.Int(nullable: false),
                        fuelTypeId = c.Int(nullable: false),
                        state = c.String(),
                    })
                .PrimaryKey(t => t.carId)
                .ForeignKey("dbo.Brands", t => t.brandId, cascadeDelete: true)
                .ForeignKey("dbo.CarTypes", t => t.carTypeId, cascadeDelete: true)
                .ForeignKey("dbo.FuelTypes", t => t.fuelTypeId, cascadeDelete: true)
                .ForeignKey("dbo.Models", t => t.modelId, cascadeDelete: true)
                .Index(t => t.carTypeId)
                .Index(t => t.brandId)
                .Index(t => t.modelId)
                .Index(t => t.fuelTypeId);
            
            CreateTable(
                "dbo.CarTypes",
                c => new
                    {
                        carTypeId = c.Int(nullable: false, identity: true),
                        description = c.String(),
                        state = c.String(),
                    })
                .PrimaryKey(t => t.carTypeId);
            
            CreateTable(
                "dbo.FuelTypes",
                c => new
                    {
                        fuelTypeId = c.Int(nullable: false, identity: true),
                        description = c.String(),
                        state = c.String(),
                    })
                .PrimaryKey(t => t.fuelTypeId);
            
            CreateTable(
                "dbo.Models",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        description = c.String(),
                        state = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        clientId = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        firstLastName = c.String(),
                        secondLastName = c.String(),
                        citizenId = c.String(),
                        creditCarNumber = c.String(),
                        creditLimit = c.Int(nullable: false),
                        personType = c.String(),
                        state = c.String(),
                    })
                .PrimaryKey(t => t.clientId);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        employeeId = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        firstLastName = c.String(),
                        secondLastName = c.String(),
                        citizenId = c.String(),
                        workshift = c.String(),
                        commissionPorcent = c.Double(nullable: false),
                        hireDate = c.DateTime(nullable: false),
                        state = c.String(),
                    })
                .PrimaryKey(t => t.employeeId);
            
            CreateTable(
                "dbo.Inspections",
                c => new
                    {
                        inspectionId = c.Int(nullable: false, identity: true),
                        hasScratches = c.Boolean(nullable: false),
                        amountOfFuel = c.String(),
                        hasResponseTire = c.Boolean(nullable: false),
                        hasHydraulicJack = c.Boolean(nullable: false),
                        hasCrystalCrashes = c.Boolean(nullable: false),
                        date = c.DateTime(nullable: false),
                        employeeId = c.Int(nullable: false),
                        status = c.String(),
                        car_carId = c.Int(),
                        client_clientId = c.Int(),
                    })
                .PrimaryKey(t => t.inspectionId)
                .ForeignKey("dbo.Cars", t => t.car_carId)
                .ForeignKey("dbo.Clients", t => t.client_clientId)
                .ForeignKey("dbo.Employees", t => t.employeeId, cascadeDelete: true)
                .Index(t => t.employeeId)
                .Index(t => t.car_carId)
                .Index(t => t.client_clientId);
            
            CreateTable(
                "dbo.Rents",
                c => new
                    {
                        id = c.Int(nullable: false),
                        comment = c.String(),
                        status = c.String(),
                        car_carId = c.Int(),
                        client_clientId = c.Int(),
                        returned = c.Boolean(),



                        employee_employeeId = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Cars", t => t.car_carId)
                .ForeignKey("dbo.Clients", t => t.client_clientId)
                .ForeignKey("dbo.Employees", t => t.employee_employeeId)
                .Index(t => t.car_carId)
                .Index(t => t.client_clientId)
                .Index(t => t.employee_employeeId);
            
            CreateTable(
                "dbo.Returns",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        rentReference_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Rents", t => t.rentReference_id)
                .Index(t => t.rentReference_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Returns", "rentReference_id", "dbo.Rents");
            DropForeignKey("dbo.Rents", "employee_employeeId", "dbo.Employees");
            DropForeignKey("dbo.Rents", "client_clientId", "dbo.Clients");
            DropForeignKey("dbo.Rents", "car_carId", "dbo.Cars");
            DropForeignKey("dbo.Inspections", "employeeId", "dbo.Employees");
            DropForeignKey("dbo.Inspections", "client_clientId", "dbo.Clients");
            DropForeignKey("dbo.Inspections", "car_carId", "dbo.Cars");
            DropForeignKey("dbo.Cars", "modelId", "dbo.Models");
            DropForeignKey("dbo.Cars", "fuelTypeId", "dbo.FuelTypes");
            DropForeignKey("dbo.Cars", "carTypeId", "dbo.CarTypes");
            DropForeignKey("dbo.Cars", "brandId", "dbo.Brands");
            DropIndex("dbo.Returns", new[] { "rentReference_id" });
            DropIndex("dbo.Rents", new[] { "employee_employeeId" });
            DropIndex("dbo.Rents", new[] { "client_clientId" });
            DropIndex("dbo.Rents", new[] { "car_carId" });
            DropIndex("dbo.Inspections", new[] { "client_clientId" });
            DropIndex("dbo.Inspections", new[] { "car_carId" });
            DropIndex("dbo.Inspections", new[] { "employeeId" });
            DropIndex("dbo.Cars", new[] { "fuelTypeId" });
            DropIndex("dbo.Cars", new[] { "modelId" });
            DropIndex("dbo.Cars", new[] { "brandId" });
            DropIndex("dbo.Cars", new[] { "carTypeId" });
            DropTable("dbo.Returns");
            DropTable("dbo.Rents");
            DropTable("dbo.Inspections");
            DropTable("dbo.Employees");
            DropTable("dbo.Clients");
            DropTable("dbo.Models");
            DropTable("dbo.FuelTypes");
            DropTable("dbo.CarTypes");
            DropTable("dbo.Cars");
            DropTable("dbo.Brands");
        }
    }
}
