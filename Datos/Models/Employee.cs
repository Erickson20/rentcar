﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Models
{
    public class Employee
    {
        public int employeeId { get; set; }
        public string name { get; set; }
        public string firstLastName { get; set; }
        public string secondLastName { get; set; }
        public string citizenId { get; set; }
        public string workshift { get; set; }
        public double commissionPorcent { get; set; }
        public DateTime hireDate { get; set; }
        public string state { get; set; }
        public int userId { get; set; }
        public User user { get; set; }
        public ICollection<Rent> rents { get; set; }
    }
}
