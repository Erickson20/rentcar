﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Models
{
    
    public class Car
    {
        public int carId { get; set; }
        public string description { get; set; }
        public int chasisNumber { get; set; }
        public int motorNumber { get; set; }
        public int plateNumber { get; set; }

      
        public int carTypeId { get; set; }
        public CarType carType { get; set; }

        public int modelId { get; set; }
        public Model model { get; set; }

        public int fuelTypeId { get; set; }
        public FuelType fuelType { get; set; }

        public string state { get; set; }
        

    }
}
