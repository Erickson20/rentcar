﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Models
{
    public class Brand
    {
        public int brandId { get; set; }
        public string description { get; set; }
        public string state { get; set; }

        public ICollection<Model> models { get; set; }
    }
}
