﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Models
{
    public class Rent
    {
        
        public int rentId { get; set; }
        public DateTime dateRent { get; set; }
        public int costByDay { get; set; }

        
        public  int rentNumber { get; set; }

        public int carId { get; set; }
        public Car car { get; set; }

        public int days { get; set; }
        public DateTime dateReturn { get; set; }

        public int clientId { get; set; }
        public Client client { get; set; }

        public int employeeId { get; set; }
        public Employee employee { get; set; }

        public bool returned { get; set; }

        public string comment { get; set; }
        public string status { get; set; }
        public float? total { get; set; }

        public ICollection<Inspection> inspections { get; set; }


    }
}
