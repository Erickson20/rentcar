﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Models
{
    public class Inspection
    {
        [Key]
        public int InspectionId { get; set; }

        public int carId { get; set; }
        public Car car { get; set; }
        public Client client { get; set; }

        public bool hasScratches { get; set; }
        public string amountOfFuel { get; set; }
        public bool hasResponseTire { get; set; }
        public bool hasHydraulicJack { get; set; }
        public bool hasCrystalCrashes { get; set; }

        public bool tyreFrontLeft { get; set; }
        public bool tyreFrontRigth { get; set; }
        public bool tyreBacktLeft { get; set; }
        public bool tyreBacktRigth { get; set; }
        public bool tyreExtra { get; set; }



        public int rentId { get; set; }
        public Rent rent { get; set; }

        public DateTime date { get; set; }

        public int employeeId { get; set; }
        public Employee employee { get; set; }

        public string InspectionType { get; set; }
        public string status { get; set; }

       
    }
}
