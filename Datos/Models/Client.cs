﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Models
{
    public class Client
    {
        public int clientId { get; set; }
        public string name { get; set; }
        public string firstLastName { get; set; }
        public string secondLastName { get; set; }
        public string citizenId { get; set; }
        public string creditCarNumber { get; set; }
        public int creditLimit { get; set; }
        public string personType { get; set; }
        public string state { get; set; }

        public ICollection<Rent> rents { get; set; }
    }
}
