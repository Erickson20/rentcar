﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Models
{
    public class Role
    {
        public int roleId { get; set; }
        public string rolName { get; set; }
        public string description { get; set; }
        

        public List<Permission> permissions { get; set; }
        public ICollection<User> users { get; set; }
    }
}
