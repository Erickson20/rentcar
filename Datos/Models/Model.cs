﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Models
{
    public class Model
    {
        public int id { get; set; }
        public string description { get; set; }
        public string state { get; set; }
        public int brandId { get; set; }
        public Brand brand { get; set; }

        public ICollection<Car> cars { get; set; }

    }
}
