﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Models
{
    public class User
    {
        public int userId { get; set; }
        public string  firstName { get; set; }
        public string lastName { get; set; }

        [Index(IsUnique = true)]
        [MaxLength(255)]
        public string email { get; set; }
        public string password { get; set; }

       
        public int roleId { get; set; }
        public Role role { get; set; }

        public string getPassword()
        {
            return Encrypter.Desncrypt(this.password);
        }

        public void setPassword(string password)
        {
            this.password = Encrypter.Encrypt(password);
        }
    }
}
